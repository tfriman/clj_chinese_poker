from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

img = Image.open("spade.jpg")
draw = ImageDraw.Draw(img)
# font = ImageFont.truetype(<font-file>, <font-size>)
#font = ImageFont.truetype("sans-serif.ttf", 16)
font = ImageFont.truetype("/System/Library/Fonts/SFNSRounded.ttf", 500)
draw.text((0, 0),"A",(5,5,5),font=font)
# draw.text((x, y),"Sample Text",(r,g,b))

img.save('AS.jpg')
