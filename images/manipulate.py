from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

def do_it(orig_filename, rank, target_file_name):
    img = Image.open(orig_filename)
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype("/System/Library/Fonts/SFNSRounded.ttf", 500)
    draw.text((0, 0), rank,(5,5,5),font=font)
    img.save(target_file_name)

def main():
    for org in ["S", "C", "H", "D"]:
        for rank in ["2","3","4","5","6","7","8","9","T","J","Q","K","A"]:
            do_it(org + ".jpg", rank, rank + org +".jpg")

if __name__== "__main__":
  main()
