# Install needed cluster tasks:

tkn hub install task git-clone

# Add pipeline account power to change target ns:
export project=chinese-poker
oc policy add-role-to-user edit system:serviceaccount:$project:pipeline -n $project
# Add secrets
## Bitbucket secret
---
apiVersion: v1
kind: Secret
type: kubernetes.io/ssh-auth
metadata:
  name: git-secret
  annotations:
    tekton.dev/git-0: bitbucket.org
data:
   ssh-privatekey: <base64 ssh private key>

## Quay secret
---
apiVersion: v1
kind: Secret
metadata:
  name: quay-secret
data:
  .dockerconfigjson: <base64 dockerconfig>
type: kubernetes.io/dockerconfigjson
