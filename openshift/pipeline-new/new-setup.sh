#! /usr/bin/env bash

set -e
# Set up everything

project=$(oc project -q)

echo "========= Gonna use ocp project '${project}', is that ok?"

read foo

oc apply -f bitbucket_secret.yaml

oc secrets link pipeline git-secret

oc apply -f quay_secret.yaml

oc secrets link pipeline quay-secret

oc apply -f pipeline-pvc.yaml

oc apply -f tasks.yaml

oc apply -f pipeline.yaml

echo -e "\n========= Ready to run?"

read bar

#oc create -f pipeline-run.yaml

tkn pipeline start chinese-poker-deploy-dev -n $project -p namespace=$project -p resultImage="image-registry.openshift-image-registry.svc:5000/$project/poker:latest" --workspace name=workspace,claimName=chinese-poker-dev-workspace  --use-param-defaults

echo -e "\n========= Running"

tkn pr logs --follow --last
