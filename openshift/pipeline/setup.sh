#! /usr/bin/env bash

set -e
# Set up everything

project=$(oc project -q)

echo "========= Gonna use ocp project '${project}', is that ok?"

read foo

oc apply -f 02_bitbucket_secret.yaml

oc secrets link pipeline git-secret

oc apply -f 03_quay_secret.yaml

oc secrets link pipeline quay-secret

oc apply -f 05_dependency_pvc.yaml

oc apply -f 07_scratchpad_pvc.yaml

oc apply -f 25_resources.yaml

oc apply -f 35_tasks.yaml

oc apply -f 36_deploy_task.yaml

oc apply -f 45_pipeline.yaml

echo -e "\n========= Ready to run?"

read bar

oc create -f 55_pipeline_run.yaml

echo -e "\n========= Running"
