#! sh

# setup argocd

echo "set up kustomize argocd for chinese poker"
oc apply -f namespace.yaml
oc apply -f argocd-imagepuller-rbac.yaml
oc apply -f argocd-poker-app-dev.yaml
oc apply -f argocd-poker-project.yaml
oc apply -f argocd-service-account-cluster-role-binding.yaml
