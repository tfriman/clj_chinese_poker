# Chinese poker application

This project was started by me years ago to learn Clojure and
ClojureScript. I feel there are some parts which should be implemented
differently but overall this was a very pleasant hobby project.

In order to play, see [Chinese poker rules](./docs/game-rules.md).

[Game flow](./docs/game-flow.md) contains a bit more technical description
what is going on.

# Development
You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

# Getting easily started

## Install justfile

https://github.com/casey/just#pre-built-binaries

Run

```shell
just list
```    

To see possible commands.

## Developing

### Easy 

Start REPL and run
```clojure
(user/start-all)
```
to start BE and FE watch builds. Code and reload!

### Setup CLJ Repl

Start repl from project.clj and load chinese_poker.core.clj in it. Eval `(user/start-app [])`. Now you have your backend running.

Start frontend watch with `(user/fe-start-watch)`. Then you can open up the CLSJ Repl

### Setup CLJS Repl

Create a Clojure Remote Repl configuration which uses .shadows-cljs/nrepl.port as nrepl port config. Start it and run:

```clojure
(shadow.cljs.devtools.api/nrepl-select :app)
```

Open up a browser in http://localhost:3000 and then you can run 
```clojure
(js/alert "foo")
```
to see an alert.

### Or start with just

Start frontend in the shell:
```shell
just fe
```

Browse to http://localhost:3000

(shadow/watch :app) ;; not needed if you've run "just fe"
(shadow/repl :app)

First browser to connect is the one having js runtime.

Inspecting re-frame app db:

re-frame.db/app-db contains the app db in CLJS side.

In js: window.re_frame.db.app_db.state

To quit, type: :cljs/quit

### Common things to do with repl:

(user/reset-persistence!)

## Running locally

This needs both backend and frontend running.

### Start backend
 ```shell
 just be
 ```

### Start frontend

```shell
just fe
```

### Using locally

Open your browser to http://localhost:3000

# Deployment and packaging

## Configuration

See [.env-template](.env-template) for configurations. TODO describe these.

## Running
https://luminusweb.com/docs/deployment.html
run:

```shell
lein uberjar
```    

And then you can run:

```shell
NREPL_PORT=7001 PORT=8080 java -jar target/uberjar/chinese-poker.jar
```

Or:

```shell
java -Dspaconfig_websocket-proto=ws -DPORT=3000 -jar target/uberjar/chinese-poker.jar
```

Logging seems to be a problem though, change evn/prod/resources/logback.xml

## Deployment to OpenShift

```shell
oc new-project chinese-poker

oc new-build --name=poker java:11 --binary=true

oc start-build poker --from-file target/uberjar/chinese-poker.jar --follow

oc new-app poker PORT=8080 NREPL=7001

oc create route edge poker \
         --service=poker \
         --insecure-policy=None

```
Tune dc if needed and rollout again

```shell
oc set env dc/poker PORT=8080 NREPL=7001
oc rollout latest poker
```

### Using ubi8 jdk11 base image

```shell
lein uberjar

oc new-build --binary --name=chinese-poker-ubi -l app=chinese-poker-ubi

oc patch bc/chinese-poker-ubi -p "{\"spec\":{\"strategy\":{\"dockerStrategy\":{\"dockerfilePath\":\"openshift/Dockerfile.jvm\"}}}}"

oc start-build chinese-poker-ubi --from-dir=. --follow

oc new-app chinese-poker-ubi PORT=8080 NREPL=7001

oc create route edge poker-ubi \
         --service=chinese-poker-ubi \
         --insecure-policy=None
```    

# Tekton Pipeline

## s2i builder

Package the builder:

```shell
cd openshift/leiningen-s2i
docker build  -t quay.io/tfriman/leiningen:jdk17 -f Dockerfile.lein .
docker push quay.io/tfriman/leiningen:jdk17
oc import-image leiningen:jdk17 --from=quay.io/tfriman/leiningen:jdk17 --confirm
```

# Configuration

## UI

See endpoint /spaconfig.json

## BE

Configuration possible using env vars

## License

MIT

Copyright © 2015- Timo Friman
