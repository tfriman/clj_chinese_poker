(defproject chinese-poker "0.1.0-SNAPSHOT"

  :dependencies [[ch.qos.logback/logback-classic "1.4.8"]
                 [cheshire "5.11.0"]
                 [cider/cider-nrepl "0.31.0"] ;; todo but needed for core handler
                 [clj-http "3.12.3"]
                 [cljs-ajax "0.8.4"]
                 [cljsjs/react "18.2.0-1"]
                 [cljsjs/react-dom "18.2.0-1"]
                 [clojure.java-time "1.2.0"]
                 [com.fasterxml.jackson.core/jackson-core "2.15.2"]
                 [com.rpl/specter "1.1.4"]
                 [cprop "0.1.19"]
                 [day8.re-frame/http-fx "0.2.4"]
                 [expectations "2.1.10"]
                 [expound "0.9.0"]
                 [funcool/struct "1.4.0"]
                 [org.keycloak/keycloak-common         "20.0.3"]
                 [org.keycloak/keycloak-core           "20.0.3"]
                 [org.keycloak/keycloak-adapter-core   "20.0.3"]
                 [org.keycloak/keycloak-authz-client   "20.0.3"]
                 [org.keycloak/keycloak-adapter-spi    "20.0.3"]
                 [org.jboss.logging/jboss-logging      "3.4.1.Final"]
                 [luminus-http-kit "0.2.0"]
                 [luminus/ring-ttl-session "0.3.3"]
                 [metosin/muuntaja "0.6.8"]
                 [metosin/reitit "0.6.0"]
                 [metosin/ring-http-response "0.9.3"]
                 [mount "0.1.17"]
                 [nrepl "1.0.0"]
                 [org.clojure/clojure "1.11.1"]
                 [org.clojure/clojurescript "1.11.60" :scope "provided"]
                 [org.clojure/tools.cli "1.0.219"]
                 [org.clojure/tools.logging "1.2.4"]
                 [org.webjars.npm/bulma "0.9.4"]
                 [org.webjars.npm/material-icons "1.13.2"]
                 [org.webjars/webjars-locator "0.47"]
                 [reagent "1.2.0"]
                 [re-frame "1.3.0"]
                 [org.clojars.bitkiller/re-frame-utils "0.1.1"] ;; adds cofx inject and track to re-frame, see https://github.com/den1k/re-frame-utils/
                 [ring-webjars "0.2.0"]
                 [ring/ring-core "1.10.0"]
                 [ring/ring-defaults "0.3.4"]
                 [ring-oauth2 "0.2.2"]
                 [selmer "1.12.58"]
                 [stylefruits/gniazdo "1.2.2"]
                 [thheller/shadow-cljs "2.24.1" :scope "provided"]
                 [com.google.javascript/closure-compiler-unshaded "v20230502"]]

  :min-lein-version "2.5.0"
  :source-paths ["src/clj" "src/cljs" "src/cljc"]
  :test-paths ["test/clj"]
  :resource-paths ["resources" "target/cljsbuild"]
  :target-path "target/%s/"
  :main ^:skip-aot chinese-poker.core
  :clean-targets ^{:protect false}
  [:target-path "target/cljsbuild"]

  ;;    [:target-path [:cljsbuild :builds :app :compiler :output-dir] [:cljsbuild :builds :app :compiler :output-to]]

  :plugins [ [lein-kibit "0.1.8"]
            [lein-cljfmt "0.8.0"]
            [lein-ancient "0.7.0"]
            [jonase/eastwood "0.9.9"]
            [lein-bikeshed "0.5.2"]
            [lein-expectations "0.0.8"]
            ]

  :profiles
  {:uberjar {
             ;; :jvm-opts ["-Xmx2G"]
             :omit-source true
             :prep-tasks ["compile" ["run" "-m" "shadow.cljs.devtools.cli" "release" "app"]]
             :aot :all
             :jvm-opts ["-Dclojure.compiler.direct-linking=true"
                        "-Dclojure.spec.skip-macros=true"]
             :uberjar-name "chinese-poker.jar"
             :source-paths ["env/prod/clj" "env/prod/cljs"]
             :resource-paths ["env/prod/resources"]}
   :dev  {
          ;; :jvm-opts ["-Dconf=dev-config.edn"]
          ;;          :prep-tasks ["resource"]
          :dependencies [[binaryage/devtools "1.0.7"]
                         ;;[cider/piggieback "0.5.3"]
                         [org.clojure/tools.namespace "1.4.4"]
                         [day8.re-frame/re-frame-10x "1.6.0"]
                         [doo "0.1.11"]
                         [pjstadig/humane-test-output "0.11.0"]
                         [prone "2021-04-23"]
                         [ring/ring-devel "1.10.0"]
                         [ring/ring-mock "0.4.0"]]
          :plugins      [[com.jakemccrary/lein-test-refresh "0.24.1"]
                         [jonase/eastwood "0.3.5"]
                         ;;[cider/cider-nrepl "0.28.3"]
                         ]

          :source-paths ["env/dev/clj" "env/dev/cljs"]
          :resource-paths ["env/dev/resources"]
          :repl-options {:init-ns chinese-poker.core}
          :injections [(require 'pjstadig.humane-test-output)
                       (pjstadig.humane-test-output/activate!)]}

   :test [:dev :project/test]
   :project/test {
                  ;;:jvm-opts ["-Dconf=test-config.edn"]
                  :resource-paths ["env/test/resources"]
                  }
   }
  :aliases {

            }
  )
