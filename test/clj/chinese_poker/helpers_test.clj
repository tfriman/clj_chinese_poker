(ns chinese-poker.helpers-test
  (:require
    [clojure.test :refer :all]
    [chinese-poker.helpers :as helpers]
    [cheshire.core :as cheshire]
    ))

(deftest test-msg-map
  (testing "simple, preserve all keywords"
    (let [in-edn {:type :x, :user :u, :foo :bar}
          in-msg-json (cheshire/encode in-edn)
          simple (helpers/msg->map {} in-msg-json)]
      (is (= in-edn simple) (str "fail: " simple))))

  (testing "leave one key as string"
    (let [in-edn {:type :x, :user :u, :foo "bar"}
          in-msg-json (cheshire/encode in-edn)
          simple (helpers/msg->map {:x #{[:foo]}} in-msg-json)]
      (is (= in-edn simple) (str "fail: " simple))))

  (testing "leave nested key as string"
    (let [in-edn {:type :x, :user :u, :foo {:bar "bar"} }
          in-msg-json (cheshire/encode in-edn)
          simple (helpers/msg->map {:x #{[:foo :bar]}} in-msg-json)]
      (is (= in-edn simple) (str "fail: " simple))))

  (testing "nested vector values"
    (let [in-edn {:type :x, :user :u, :foo [{:boo :hoo}]}
          in-msg-json (cheshire/encode in-edn)
          simple (helpers/msg->map in-msg-json)]
      (is (= in-edn simple) (str "fail: " simple))))

  (testing "more nested vector values"
    (let [in-edn {:type :x, :user :u, :game {:moves {:u [{:display :8D} {:display :AD}]}}}
          in-msg-json (cheshire/encode in-edn)
          simple (helpers/msg->map in-msg-json)]
      (is (= in-edn simple) (str "fail: " simple))))

  (testing "leave numbers as is"
    (let [in-edn {:type :x, :user :u, :number1 1}
          in-msg-json (cheshire/encode in-edn)
          simple (helpers/msg->map in-msg-json)]
      (is (= in-edn simple) (str "fail: " simple))))

  (testing "leave numbers as is, list"
    (let [in-edn {:type :x, :user :u, :number1 [1 2]}
          in-msg-json (cheshire/encode in-edn)
          simple (helpers/msg->map in-msg-json)]
      (is (= in-edn simple) (str "fail: " simple))))
  )

(deftest test-json-strings
  (testing "just simple json"
    (let [in-string-json "{\"foo\":\"bar\"}"
          json-map (helpers/json-string->map in-string-json)]
      (is (= {:foo "bar"} json-map))
      ))
  (testing "complex json"
    (let [in-string-json "{\"foo\":{\"zop\":\"bar\"}}"
          json-map (helpers/json-string->map in-string-json)]
      (is (= {:foo {:zop "bar"}} json-map))
      ))
  )
