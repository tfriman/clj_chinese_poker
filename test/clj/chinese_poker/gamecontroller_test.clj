(ns chinese-poker.gamecontroller-test
  (:require
   [clojure.test :refer :all]
   [chinese-poker.gamecontroller :refer :all]
   [chinese-poker.poker.valuator :as valuator]
   [clojure.tools.logging :as log]
   ))

(def toinen-voittaa-kaikki-yksi-validi {:creator "a", :id :8317f05b-5117-4cc2-a265-0111985e2737, :state :created, :created #inst "2020-07-30T10:52:51.658-00:00", :players ["a" "b"], :game {:playermapping {:a :p1, :b :p2}, :idtoplayermapping {:p1 :a, :p2 :b}, :dealt {:p1 '({:suit :club, :rank :7, :display "7C"} {:suit :heart, :rank :k, :display "KH"} {:suit :heart, :rank :5, :display "5H"} {:suit :heart, :rank :a, :display "AH"} {:suit :club, :rank :3, :display "3C"} {:suit :spade, :rank :8, :display "8S"} {:suit :heart, :rank :3, :display "3H"} {:suit :spade, :rank :4, :display "4S"} {:suit :spade, :rank :a, :display "AS"} {:suit :diamond, :rank :q, :display "QD"} {:suit :diamond, :rank :t, :display "TD"} {:suit :heart, :rank :t, :display "TH"} {:suit :spade, :rank :9, :display "9S"}), :p2 '({:suit :club, :rank :j, :display "JC"} {:suit :heart, :rank :7, :display "7H"} {:suit :club, :rank :2, :display "2C"} {:suit :spade, :rank :k, :display "KS"} {:suit :diamond, :rank :a, :display "AD"} {:suit :club, :rank :4, :display "4C"} {:suit :heart, :rank :6, :display "6H"} {:suit :diamond, :rank :2, :display "2D"} {:suit :club, :rank :q, :display "QC"} {:suit :club, :rank :a, :display "AC"} {:suit :diamond, :rank :8, :display "8D"} {:suit :diamond, :rank :7, :display "7D"} {:suit :diamond, :rank :k, :display "KD"})}, :moves {:p1 {:u {:cards [{:suit :heart, :rank :a, :display "AH"} {:suit :heart, :rank :k, :display "KH"} {:suit :heart, :rank :5, :display "5H"} {:suit :spade, :rank :a, :display "AS"} {:suit :heart, :rank :t, :display "TH"}]}, :m {:cards [{:suit :club, :rank :7, :display "7C"} {:suit :spade, :rank :8, :display "8S"} {:suit :spade, :rank :4, :display "4S"} {:suit :diamond, :rank :q, :display "QD"} {:suit :spade, :rank :9, :display "9S"}]}, :l {:cards [{:suit :club, :rank :3, :display "3C"} {:suit :heart, :rank :3, :display "3H"} {:suit :diamond, :rank :t, :display "TD"}]}}, :p2 {:u {:cards [{:suit :diamond, :rank :a, :display "AD"} {:suit :spade, :rank :k, :display "KS"} {:suit :club, :rank :j, :display "JC"} {:suit :club, :rank :q, :display "QC"} {:suit :club, :rank :a, :display "AC"}]}, :m {:cards [{:suit :heart, :rank :7, :display "7H"} {:suit :heart, :rank :6, :display "6H"} {:suit :diamond, :rank :8, :display "8D"} {:suit :diamond, :rank :7, :display "7D"} {:suit :diamond, :rank :k, :display "KD"}]}, :l {:cards [{:suit :club, :rank :2, :display "2C"} {:suit :club, :rank :4, :display "4C"} {:suit :diamond, :rank :2, :display "2D"}]}}}}})

(def toinen-voittaa-kaikki-kaksi-validia {:creator "a", :id :8317f05b-5117-4cc2-a265-0111985e2737, :state :created, :created #inst "2020-07-30T10:52:51.658-00:00", :players ["a" "b"], :game {:playermapping {:a :p1, :b :p2}, :idtoplayermapping {:p1 :a, :p2 :b}, :dealt {:p1 '({:suit :club, :rank :7, :display "7C"} {:suit :heart, :rank :k, :display "KH"} {:suit :heart, :rank :5, :display "5H"} {:suit :heart, :rank :a, :display "AH"} {:suit :club, :rank :3, :display "3C"} {:suit :spade, :rank :8, :display "8S"} {:suit :heart, :rank :3, :display "3H"} {:suit :spade, :rank :4, :display "4S"} {:suit :spade, :rank :a, :display "AS"} {:suit :diamond, :rank :q, :display "QD"} {:suit :diamond, :rank :t, :display "TD"} {:suit :heart, :rank :t, :display "TH"} {:suit :spade, :rank :9, :display "9S"}), :p2 '({:suit :club, :rank :j, :display "JC"} {:suit :heart, :rank :7, :display "7H"} {:suit :club, :rank :2, :display "2C"} {:suit :spade, :rank :k, :display "KS"} {:suit :diamond, :rank :a, :display "AD"} {:suit :club, :rank :4, :display "4C"} {:suit :heart, :rank :6, :display "6H"} {:suit :diamond, :rank :2, :display "2D"} {:suit :club, :rank :q, :display "QC"} {:suit :club, :rank :a, :display "AC"} {:suit :diamond, :rank :8, :display "8D"} {:suit :diamond, :rank :7, :display "7D"} {:suit :diamond, :rank :k, :display "KD"})}, :moves {:p1 {:u {:cards [{:suit :heart, :rank :a, :display "AH"} {:suit :heart, :rank :k, :display "KH"} {:suit :heart, :rank :5, :display "5C"} {:suit :spade, :rank :a, :display "AS"} {:suit :heart, :rank :t, :display "TH"}]}, :m {:cards [{:suit :club, :rank :7, :display "7C"} {:suit :spade, :rank :8, :display "8S"} {:suit :spade, :rank :4, :display "4S"} {:suit :diamond, :rank :q, :display "QD"} {:suit :spade, :rank :9, :display "9S"}]}, :l {:cards [{:suit :club, :rank :3, :display "3C"} {:suit :heart, :rank :5, :display "5H"} {:suit :diamond, :rank :t, :display "TD"}]}}, :p2 {:u {:cards [{:suit :diamond, :rank :a, :display "AD"} {:suit :spade, :rank :k, :display "KS"} {:suit :club, :rank :j, :display "JC"} {:suit :club, :rank :q, :display "QC"} {:suit :club, :rank :a, :display "AC"}]}, :m {:cards [{:suit :heart, :rank :7, :display "7H"} {:suit :heart, :rank :6, :display "6H"} {:suit :diamond, :rank :8, :display "8D"} {:suit :diamond, :rank :7, :display "7D"} {:suit :diamond, :rank :k, :display "KD"}]}, :l {:cards [{:suit :club, :rank :2, :display "2C"} {:suit :club, :rank :4, :display "4C"} {:suit :diamond, :rank :2, :display "2D"}]}}}}})

(deftest test-winning-hand-calculation
  (testing "first wins all hands"
    (is (= {:u '(:p2), :m '(:p2), :l '(:p2)} (:winners (get-results-for-game (:game toinen-voittaa-kaikki-kaksi-validia)))))
    ;; todo add more tests
    )
  )

(deftest test-valid-hands
  (testing "no valid hands"
    (log/info "no valid hands called")
    (is (empty?
         (filter get-valid-hands
                 (list (first (map second
                                   (get-in toinen-voittaa-kaikki-yksi-validi [:game :moves])))
                       )
                 ))))
  (testing "one valid hand"
    (is (= 1 (count (filter get-valid-hands
                            (map second
                                 (get-in toinen-voittaa-kaikki-yksi-validi [:game :moves]))
                            )))))
  (testing "two valid hands"
    (is (= 2 (count
              (filter get-valid-hands
                      (map second
                           (get-in toinen-voittaa-kaikki-kaksi-validia [:game :moves]))
                      )
              )
           ))
    ))

(defn map-hand-to-strength [h]
  (let [hc (map :display h)
        v (valuator/value hc)]
    [hc v]
    ))

(comment
  (map map-hand-to-strength (map :cards (flatten (map vals (map #(select-keys % [:u :m :l]) (map second (:moves (:game toinen-voittaa-kaikki-yksi-validi))))))))
  (map map-hand-to-strength (map :cards (flatten (map vals (map #(select-keys % [:u :m :l]) (map second (:moves (:game toinen-voittaa-kaikki-kaksi-validia))))))))
  (get-valid-hands (second (first    (get-in toinen-voittaa-kaikki-yksi-validi [:game :moves]))))
  )

(def h-straight-flush ["5S" "8S" "9S" "7S" "6S"])
(def h-4-of-a-kind ["2S" "2D" "2H" "2C" "3S"])
(def h-full-house-TTT99 ["9S" "TS" "TH" "TC" "9C"])
(def h-full-house-999QQ ["QS" "9S" "9C" "9H" "QC"])
(def h-full-house-999TT ["9S" "TS" "TH" "9H" "9C"])
(def h-flush ["2S" "5S" "7S" "9S" "3S"])
(def h-straight ["2S" "3S" "4S" "6H" "5S"])
(def h-3-of-a-kind ["2S" "5D" "2H" "2C" "3S"])
(def h-two-pairs ["2S" "5D" "5H" "2C" "3S"])
(def h-pair ["2S" "5D" "7H" "2C" "3S"])
(def h-hi-hand ["2S" "5D" "7H" "AC" "3S"])
(def h-hi-hand-low ["2S" "5D" "7H" "8C" "3S"])

(def l-hi-hand ["2S" "KD" "AH"])
(def l-hi-hand-low ["2S" "5D" "3H"])
(def l-pair-hand ["5S" "5D" "7H"])
(def l-three-of-a-kind-hand ["5S" "5D" "5H"])

(defn- ->hvaluestomaps [hvs]
  (mapv (fn [x] {:display x}) hvs))

(defn- ->hand-set
  [u m l]
  {:u {:cards (->hvaluestomaps u)}
   :m {:cards (->hvaluestomaps m)}
   :l {:cards (->hvaluestomaps l)}})

(def valid-hand-1 (->hand-set h-straight-flush h-flush l-pair-hand))
(def valid-hand-2 (->hand-set h-flush h-two-pairs l-hi-hand))
(def invalid-hand-1 (->hand-set h-flush h-straight-flush l-hi-hand)) ;; u < m
(def invalid-hand-2 (->hand-set h-flush h-two-pairs l-three-of-a-kind-hand)) ;; m < l 3
(def invalid-hand-3 (->hand-set h-flush h-pair l-pair-hand));; m < l pair
(def invalid-hand-4 (->hand-set h-flush h-hi-hand l-hi-hand)) ;; m < l hi

(defn- get-winners
  "gets winners"
  [gamestate hand]
  ;;(prn "get-winners")
  (->> gamestate
       :winners
       hand
       ))

(deftest test-handle-valid-hands
  (testing "only one valid hand"
    (let [game-state {:moves {:p1 (->hand-set h-full-house-TTT99 h-flush l-hi-hand)}}
          valid-hands [:p1]]
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :u)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :m)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :l)))
      ))
  (testing "two valid hands, one wins all"
    (let [game-state {:moves {:p1 (->hand-set h-full-house-TTT99 h-flush l-pair-hand)
                             :p2 (->hand-set  h-hi-hand  h-hi-hand l-hi-hand-low)}}
          valid-hands [:p1 :p2]]
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :u)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :m)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :l)))))
  (testing "two valid hands, one wins 2"
    (let [game-state {:moves {:p1 (->hand-set h-full-house-TTT99 h-flush l-hi-hand-low)
                             :p2 (->hand-set h-hi-hand h-hi-hand l-pair-hand)}}
          valid-hands [:p1 :p2]]
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :u)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :m)))
      (is (= [:p2] (get-winners (handle-valid-hands game-state valid-hands) :l)))))
  (testing "two valid hands, one wins 2, one is tied"
    (let [game-state {:moves {:p1 (->hand-set h-full-house-TTT99 h-flush l-hi-hand)
                             :p2 (->hand-set h-hi-hand h-hi-hand l-hi-hand)}}
          valid-hands [:p1 :p2]]
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :u)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :m)))
      (is (= #{:p2 :p1} (set (get-winners (handle-valid-hands game-state valid-hands) :l))))))
  (testing "two valid hands, one wins 2, last one hi hands"
    (let [game-state {:moves {:p1 (->hand-set h-full-house-TTT99 h-flush l-hi-hand)
                             :p2 (->hand-set h-hi-hand h-hi-hand l-hi-hand-low)}}
          valid-hands [:p1 :p2]]
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :u)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :m)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :l)))))
  (testing "two valid hands, one wins 2, middle one hi hands"
    (let [game-state {:moves {:p1 (->hand-set h-full-house-TTT99 h-hi-hand-low l-hi-hand)
                             :p2 (->hand-set h-hi-hand h-hi-hand l-hi-hand-low)}}
          valid-hands [:p1 :p2]]
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :u)))
      (is (= [:p2] (get-winners (handle-valid-hands game-state valid-hands) :m)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :l)))))
  (testing "two valid hands, hi both fulls, one wins 2, middle one hi hands"
    (let [game-state {:moves {:p1 (->hand-set h-full-house-TTT99 h-hi-hand-low l-hi-hand)
                              :p2 (->hand-set h-full-house-999TT h-hi-hand l-hi-hand-low)}}
          valid-hands [:p1 :p2]]
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :u)))
      (is (= [:p2] (get-winners (handle-valid-hands game-state valid-hands) :m)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :l)))))
  (testing "two valid hands, hi both fulls, one wins 2, middle one hi hands"
    (let [game-state {:moves {:p1 (->hand-set h-full-house-999QQ h-hi-hand-low l-hi-hand)
                              :p2 (->hand-set h-full-house-999TT h-hi-hand l-hi-hand-low)}}
          valid-hands [:p1 :p2]]
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :u)))
      (is (= [:p2] (get-winners (handle-valid-hands game-state valid-hands) :m)))
      (is (= [:p1] (get-winners (handle-valid-hands game-state valid-hands) :l)))))
  )

(deftest test-hand-valid
  (testing "check if hands are valid"
    (is (= true (is-valid-hand-set? valid-hand-1)))
    (is (= false (is-valid-hand-set? invalid-hand-1)))
    (is (= false (is-valid-hand-set? invalid-hand-2)))
    (is (= false (is-valid-hand-set? invalid-hand-3)))
    (is (= false (is-valid-hand-set? invalid-hand-4)))
    (is (= false (is-valid-hand-set? (->hand-set h-flush h-flush l-hi-hand))))
    ))

(deftest test-get-valid-hands
  (testing "get valid hands"
    (is (empty? (filter get-valid-hands [])))
    (is (= 1 (count (filter get-valid-hands [valid-hand-1]))))
    (is (= 1 (count (filter get-valid-hands [valid-hand-1 invalid-hand-1 invalid-hand-1]))))
    (is (= 4 (count (filter get-valid-hands [valid-hand-1 valid-hand-1 valid-hand-1 valid-hand-1 ]))))
    ))

(deftest test-same-hand-class
  (is (= [:p1] (handle-same-hand-class [[:p1 (->hvaluestomaps ["2s" "5D" "AH"]) 0] [:p2 (->hvaluestomaps  ["2s" "5D" "3H"]) 0]] 0)))
  (is (= #{:p1 :p2} (set (handle-same-hand-class [[:p1 (->hvaluestomaps ["2s" "5D" "AH"]) 0] [:p2 (->hvaluestomaps  ["2s" "5D" "AH"]) 0]] 0))))
  )

