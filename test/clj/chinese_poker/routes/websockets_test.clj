(ns chinese-poker.routes.websockets-test
  (:require
   [clojure.test :refer :all]
   [chinese-poker.core]
   [gniazdo.core :as ws-lib]
   [mount.core :as mount]
   [com.rpl.specter :as specter]
   [clojure.tools.logging :as log]
   [chinese-poker.helpers :as helpers]))

(def outgoing (atom [])) ;; todo remove me

(def test-map {:tournaments [{:id :a1 :val 1 :val2 [:cxxx :ff]}
                            {:id :a2 :players [:p1 :p2] :val 2 :val2 []}]
              :foo          []})
(defn is-key? [m]
  (= :a2 m))

(defn is-key-fn? [x]
  (fn [m]
    (= x m)))

(def testtransform (specter/transform [:tournaments specter/ALL (specter/if-path [:id is-key?] :val)] (fn [_] :bbb) test-map))

(def testtransform1 (specter/transform [:tournaments specter/ALL (specter/if-path [:id (is-key-fn? :a1)] :val2)] (fn [l] (conj l :bbb )) test-map))

(def testtransform2 (specter/transform [:tournaments specter/ALL (specter/if-path [:id (is-key-fn? :a1)] :val777)] (fn [l] (conj l :bbb)) test-map))

(def selecttest (specter/select [:tournaments specter/ALL (specter/if-path [:id (is-key-fn? :a2)] [:players specter/ALL])] test-map))

(def testtransform3 (specter/transform [:tournaments specter/ALL (specter/if-path [:id (is-key-fn? :a1)] :val777)] (fn [l] ((fnil conj []) l :bbb )) test-map))
;;(alter-meta! *ns* assoc ::load false)
(def port 3003)

(def token-player1 "p1-token")
(def token-player2 "p2-token")

(def auth-map {:p1-token {:user :player1
                          :email "p1@m.invalid"}
               :p2-token {:user :player2
                          :email "p2@m.invalid"}
               })
(def authentication-mock {:start (fn []
                                   {:verify-fn  (fn [token]
                                                       (log/info "verify fn" token)
                                                       (keyword token))
                                         :extract-fn (fn [access-token]
                                                       (log/info "extract fn" access-token)
                                                       (access-token auth-map)
                                                       )})})

(def mappings-p1 {:u [:TC :2C :TD :2H :JH]
                  :m [:7S :8D :JS :9C :KS]
                  :l [:4D :6D :5H]})

(def mappings-p1-fullhouse {:u [:TC :2C :TD :2H :TS]
                            :m [:7S :8D :JS :9C :KS]
                            :l [:4D :6D :5H]})

(def mappings-p2 {:u [:AH :AC :3C :3H :8H]
                  :m [:8S :KD :JC :5C :8C]
                  :l [:6C :6S :TH]})

(def fixed-game {:start (fn []
                          (let [counter (atom -1)]
                            (fn [_ _ roundnumber]
                              (let [responses [{:playermapping {:player1 :p1, :player2 :p2},
                                                :idtoplayermapping {:p1 :player1, :p2 :player2},
                                                :dealt
                                                {:p1
                                                 [{:display :2C}
                                                  {:display :TC}
                                                  {:display :4D}
                                                  {:display :TD}
                                                  {:display :2H}
                                                  {:display :7S}
                                                  {:display :6D}
                                                  {:display :8D}
                                                  {:display :JS}
                                                  {:display :5H}
                                                  {:display :9C}
                                                  {:display :KS}
                                                  {:display :JH}],
                                                 :p2
                                                 [{:display :8S}
                                                  {:display :6C}
                                                  {:display :3C}
                                                  {:display :AH}
                                                  {:display :AC}
                                                  {:display :3H}
                                                  {:display :KD}
                                                  {:display :6S}
                                                  {:display :JC}
                                                  {:display :TH}
                                                  {:display :5C}
                                                  {:display :8C}
                                                  {:display :8H}]},
                                                :moves {:p1 {:u {:cards []}
                                                             :m {:cards []}
                                                             :l {:cards []}
                                                             }
                                                        :p2 {:u {:cards []}
                                                             :m {:cards []}
                                                             :l {:cards []}
                                                             }
                                                        }}
                                               ;; second
                                               {:playermapping {:player1 :p1, :player2 :p2},
                                                :idtoplayermapping {:p2 :player2, :p1 :player1},
                                                :dealt
                                                {:p1
                                                 [{:display :2C}
                                                  {:display :TC}
                                                  {:display :4D}
                                                  {:display :TD}
                                                  {:display :2H}
                                                  {:display :7S}
                                                  {:display :6D}
                                                  {:display :8D}
                                                  {:display :JS}
                                                  {:display :5H}
                                                  {:display :9C}
                                                  {:display :KS}
                                                  {:display :TS}],
                                                 :p2
                                                 [{:display :8S}
                                                  {:display :6C}
                                                  {:display :3C}
                                                  {:display :AH}
                                                  {:display :AC}
                                                  {:display :3H}
                                                  {:display :KD}
                                                  {:display :6S}
                                                  {:display :JC}
                                                  {:display :TH}
                                                  {:display :5C}
                                                  {:display :8C}
                                                  {:display :8H}]},
                                                :moves {:p1 {:u {:cards []}
                                                             :m {:cards []}
                                                             :l {:cards []}
                                                             }
                                                        :p2 {:u {:cards []}
                                                             :m {:cards []}
                                                             :l {:cards []}
                                                             }
                                                        }}
                                               ]
                                    c (swap! counter inc)
                                    ]
                                (when (not= roundnumber c)
                                  (throw (AssertionError. (str "Round numbers do not match: " roundnumber " and " c))))
                                (nth responses c)
                                ))))})
(use-fixtures
  :each
  (fn [f]
    (log/info "STARTING FIXTURES: " port)
    (-> (mount/with-args {:dev        true
                          :port       port
                          :nrepl-port nil
                          })
        (mount/only [#'chinese-poker.config/env
                     #'chinese-poker.handler/init-app
                     #'chinese-poker.engine/create-game
                     #'chinese-poker.authentication.keycloak-v2/keycloak-deployment
                     #'chinese-poker.handler/app-routes
                     #'chinese-poker.core/http-server])

        (mount/swap-states {#'chinese-poker.engine/create-game                             fixed-game
                            #'chinese-poker.authentication.keycloak-v2/keycloak-deployment authentication-mock})
        mount/start)
    (f)
    (mount/stop)))

(def type-map
  "Contains fields which should not be mapped to keywords"
  ;; TODO this shows mapping is not perfect.
  {:bf-game-ended #{[:player1] [:player2] [:player3] [:player4]}})

(defn- socket
  "Create socket connection. Store responses to a (atom)"
  [a]
  (ws-lib/connect (str "ws://localhost:" port "/ws")
                  :on-receive (fn [msg] (swap! a conj (helpers/msg->map type-map msg)))
                  :on-connect #(prn 'connected %)))

(defn test-send [socket]
  (fn [msg]
    (swap! outgoing conj msg)
    (ws-lib/send-msg socket (helpers/msg-as-json msg))
    (Thread/sleep 50)))

(defn- response-helper
  "Responses are arrays so take latest and get key from that."
  ([atom-response key]
   (response-helper atom-response key identity))
  ([atom-response key f]
   (let [res (-> (deref atom-response)
                 f
                 last
                 key)]
     (log/debug "RESPONSE-HELPER " key " RES " res)
     res)))

(defn- check-tournament-update
  "Check tournament update result against expected map"
  [{:keys [previous totalpoints] :as update} {:keys [expected-previous expected-totalpoints]}]
  (try
    (is (= expected-previous previous))
    (is (= expected-totalpoints totalpoints))
    (catch Throwable t
      (log/errorf "check tour xxx failed for %s" update)
      (throw t))))

(defn- resolve-mapping
  "Helper to solve where to put a given card.
  Returns one of the :u :m :l"
  [hand-mapping card]
  (comment
    ;;   hand-mapping example:
    {:u [:TC :2C :TD :2H] :l [:4D]}
    )
  (if-let [result (->> hand-mapping
                       (filter (fn [[_ b]] (some #{card} b)))
                       ffirst)]
    result
    (do
      (log/error "card:" card " no mapping in " hand-mapping)
      (throw (Exception. "card mapping failure")))))

(defn- cards-to-hands
  "Helper to resolve cards from message and then get list of card mappings
   Create {:u [cards] :m [cards...] :l [:cards]}"
  [req hand-mapping]
  (log/info "cards-to-hands " req " and mapping" hand-mapping)
  (def req req)
  (def hand-mapping hand-mapping)
  (reduce (fn [acc c]
            (update-in acc [(resolve-mapping hand-mapping (:display c)) :cards] #(conj % c))
            )
          {:u {:cards []} :m {:cards []} :l {:cards []}}
          (:cards req))
  )

(defn- run-game
  "runs game"
  [{{sender1 :sender player1 :player hand-mapping1 :mapping ws-response1 :responses} :p1
    {sender2 :sender player2 :player hand-mapping2 :mapping ws-response2 :responses} :p2
    tournament-id                                                                    :tournament-id}]
  (sender1 {:user          player1
            :type          :fb-start-game
            :tournament-id tournament-id})

  (def game-id (response-helper ws-response1 :game-id))

  (is (some? game-id))

  (is (some? (response-helper ws-response1 :cards)))

  (defn do-sender [sender {:keys [mapping responses] :as m}]
    (let [inputmap (select-keys m [:user :tournament-id :game-id :type])]
      (fn []
        (sender (assoc inputmap :cards (cards-to-hands (last (deref responses)) mapping))))))

  (def automatic-sender1 (do-sender sender1 {:user          player1
                                             :tournament-id tournament-id
                                             :game-id       game-id
                                             :type          :fb-cards-placed
                                             :mapping       hand-mapping1
                                             :responses     ws-response1
                                             }))

  (def automatic-sender2 (do-sender sender2 {:user          player2
                                             :tournament-id tournament-id
                                             :game-id       game-id
                                             :type          :fb-cards-placed
                                             :mapping       hand-mapping2
                                             :responses     ws-response2
                                             }))

  (doall (repeatedly 9 (fn [] (automatic-sender1) (automatic-sender2)))))

(deftest websocket-test-p2-wins-all-two-games
  (let [ws-response1 (atom [])
        ws-response2 (atom [])
        _ (def ws1 ws-response1)
        _ (def ws2 ws-response2)
        socket1 (socket ws-response1)
        socket2 (socket ws-response2)
        sender1 (test-send socket1)
        sender2 (test-send socket2)]
    (try
      (sender1 {:type  :fb-credentials
                :user  :player1
                :token token-player1})

      (sender2 {:type  :fb-credentials
                :user  :player2
                :token token-player2})

      (sender1 {:user :player1
                :type :fb-create-tournament
                :name "unittesttournament"})

      (def tournament-id (response-helper ws-response1 :id))

      (is (some? tournament-id))

      (sender1 {:user          :player1
                :type          :fb-join-tournament
                :tournament-id tournament-id})

      (sender2 {:user          :player2
                :type          :fb-join-tournament
                :tournament-id tournament-id})

      (sender1 {:user          :player1
                :type          :fb-start-tournament
                :tournament-id tournament-id})

      (def run-fixture-default {:p1 {:sender sender1
                                     :mapping mappings-p1
                                     :responses ws-response1
                                     :player :player1}
                                :p2 {:sender sender2
                                     :mapping mappings-p2
                                     :responses ws-response2
                                     :player :player2}
                                :tournament-id tournament-id})

      (run-game run-fixture-default)

      (is (= :bf-game-ended (response-helper ws-response1 :type drop-last)))
      (let [update (last @ws-response1)]
        (is (= :bf-tournament-update (:type update)))
        (check-tournament-update update
                                 {:expected-previous {:u [:p2]
                                                      :m [:p2]
                                                      :l [:p2]
                                                      :points {:player2 3}}
                                  :expected-totalpoints {:player2 3}}))
      (is (= (last @ws-response1) (last @ws-response2)))

      (run-game (assoc-in run-fixture-default [:p1 :mapping] mappings-p1-fullhouse))

      (is (= :bf-game-ended (response-helper ws-response1 :type drop-last)))
      (let [update (last @ws-response1)]
        (is (= :bf-tournament-update (:type update)))
        (check-tournament-update update
                                 {:expected-previous {:u [:p1]
                                                      :m [:p2]
                                                      :l [:p2]
                                                      :points {:player1 1 :player2 2}}
                                  :expected-totalpoints {:player2 5 :player1 1}}))

      (is (= (last @ws-response1) (last @ws-response2)))

      (catch Throwable t
        (log/error "throwable!" t)
        (log/error "LAST RESULT 1" (last @ws-response1))
        (log/error "LAST RESULT 2" (last @ws-response2))
        (is false))
      (finally
        (ws-lib/close socket1)
        (ws-lib/close socket2)))))

(deftest websocket-test-p2-wins-all
  (let [wsresponse1 (atom [])
        wsresponse2 (atom [])
        _ (def wsresponse1 wsresponse1)
        socket1 (socket wsresponse1)
        socket2 (socket wsresponse2)
        sender1 (test-send socket1)
        sender2 (test-send socket2)]
    (try
      (sender1 {:type :fb-credentials
                :user :player1
                :token token-player1
                })

      (sender2 {:type :fb-credentials
                :user :player2
                :token token-player2})

      (sender1 {:user :player1
                :type :fb-create-tournament
                :name "unittesttournament"})

      (def tournament-id (response-helper wsresponse1 :id))

      (is (some? tournament-id))

      (sender1 {:user          :player1
                :type          :fb-join-tournament
                :tournament-id tournament-id})

      (sender2 {:user          :player2
                :type          :fb-join-tournament
                :tournament-id tournament-id})

      (sender1 {:user          :player1
                :type          :fb-start-tournament
                :tournament-id tournament-id})

      (sender1 {:user          :player1
                :type          :fb-start-game
                :tournament-id tournament-id})

      (def run-fixture-default {:p1 {:sender sender1
                                     :mapping mappings-p1
                                     :responses wsresponse1
                                     :player :player1}
                                :p2 {:sender sender2
                                     :mapping mappings-p2
                                     :responses wsresponse2
                                     :player :player2}
                                :tournament-id tournament-id})
      (run-game run-fixture-default)

      (is (= :bf-game-ended (response-helper wsresponse1 :type drop-last)))
      (let [update (last @wsresponse1)]
        (is (= :bf-tournament-update (:type update)))
        (check-tournament-update update
                                 {:expected-previous {:u [:p2]
                                                      :m [:p2]
                                                      :l [:p2]
                                                      :points {:player2 3}}
                                  :expected-totalpoints {:player2 3}}))

      (is (= (last @wsresponse1) (last @wsresponse2)))
      (catch Throwable t
        (log/error "throwable!" t)
        (log/error "LAST RESULT 1" @wsresponse1)
        (log/error "LAST RESULT 2" @wsresponse2))
      (finally
        (ws-lib/close socket1)
        (ws-lib/close socket2)))))

(deftest websocket-test-3-sockets-2-receive
  (let [ws-response1 (atom [])
        ws-response2 (atom [])
        ws-response3 (atom [])
        socket1 (socket ws-response1)
        socket2 (socket ws-response2)
        socket3 (socket ws-response3)
        sender1 (test-send socket1)
        sender2 (test-send socket2)]
    (try
      (sender1 {:type :fb-credentials
                :user :player1
                :token token-player1})

      (sender2 {:type :fb-credentials
                :user :player2
                :token token-player2})

      (sender1 {:user :player1
                :type :fb-create-tournament
                :name "unittesttournament"})

      (def tournament-id (response-helper ws-response1 :id))

      (is (some? tournament-id))

      (sender1 {:user          :player1
                :type          :fb-join-tournament
                :tournament-id tournament-id})

      (sender2 {:user          :player2
                :type          :fb-join-tournament
                :tournament-id tournament-id})

      (sender1 {:user          :player1
                :type          :fb-start-tournament
                :tournament-id tournament-id})

      (is (= (last @ws-response1) (last @ws-response2)))
      (is (= :bf-tournament-started (response-helper ws-response1 :type)))
      (is (= :bf-tournament-started (response-helper ws-response2 :type)))
      (is (= :bf-tournament-created (response-helper ws-response3 :type)))
      (catch Throwable t
        (log/error "throwable!" t)
        (log/error "LAST RESULT 1" @ws-response1)
        (log/error "LAST RESULT 2" @ws-response2))
      (finally
        (ws-lib/close socket1)
        (ws-lib/close socket2)))))
