(ns chinese-poker.engine-test
  (:require
   [clojure.test :refer :all]
   [chinese-poker.engine :refer :all]
   ))

(defn- gen-card [v]
  {:display v}
  )

(comment
  {:p1
   {:u {:cards [{:display "AD"} {:display "AS"}]},
    :m {:cards [{:display "KS"}]},
    :l {:cards [{:display "TS"} {:display "2S"}]}},
   :p2 {:u {:cards []}, :m {:cards []}, :l {:cards []}}}
  )

(deftest test-order
  (testing "first guys turn"
    (let [input {:moves {:p1 {:u {:cards [(gen-card "AS")]}
                              :m {:cards []}
                              :l {:cards []}
                              }
                         :p2 {:u {:cards [(gen-card "AH")]}
                              :m {:cards []}
                              :l {:cards []}
                              }
                         }}
          {:keys [player cardcount]} (find-next-player-and-cards input)]
      (is (= :p1 player))
      (is (= 1 cardcount))
      ))

  (testing "third guys turn"
    (let [input {:moves {:p1 {:u {:cards [(gen-card "AS")]}
                              :m {:cards [(gen-card "AS")]}
                              :l {:cards []}
                              }
                         :p2 {:u {:cards [(gen-card "AH")]}
                              :m {:cards [(gen-card "AS")]}
                              :l {:cards []}
                              }
                         :p3 {:u {:cards []}
                              :m {:cards []}
                              :l {:cards []}
                              }
                         :p4 {:u {:cards []}
                              :m {:cards []}
                              :l {:cards [(gen-card "AS") (gen-card "AS")]}
                              }
                         }}
          {:keys [player cardcount]} (find-next-player-and-cards input)]
      (is (= :p3 player))
      (is (= 0 cardcount))
      ))

  (testing "fourth guys turn"
    (let [input {:moves {:p1 {:u {:cards [(gen-card "AS")]}
                              :m {:cards [(gen-card "AS")]}
                              :l {:cards []}
                              }
                         :p2 {:u {:cards [(gen-card "AH")]}
                              :m {:cards [(gen-card "AS")]}
                              :l {:cards []}
                              }
                         :p3 {:u {:cards [(gen-card "2H")]}
                              :m {:cards []}
                              :l {:cards []}
                              }
                         :p4 {:u {:cards []}
                              :m {:cards []}
                              :l {:cards []}
                              }

                         }}
          {:keys [player cardcount]} (find-next-player-and-cards input)]
      (is (= :p4 player))
      (is (= 0 cardcount))
      ))
  )
