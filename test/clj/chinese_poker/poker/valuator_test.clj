(ns chinese-poker.poker.valuator-test
  (:require
   [clojure.test :refer :all]
   [chinese-poker.poker.valuator :refer :all]
   ))

(def h-straight-flush [:5S :8S :9S :7S :6S])
(def h-4-of-a-kind-23 [:2S :2D :2H :2C :3S])
(def h-4-of-a-kind-24 [:2S :2D :2H :2C :4S])
(def h-4-of-a-kind-32 [:3S :3D :3H :3C :2S])
(def h-4-of-a-kind-A2 [:AS :AD :AH :AC :2S])
(def h-4-of-a-kind-KQ [:KS :KD :KH :KC :QS])
(def h-full-house-999TT [:9S :TS :TH :9H :9C])
(def h-full-house-999QQ [:QS :9S :9D :9H :QC])
(def h-full-house-TTT22 [:2S :TS :TH :9H :2C])
(def h-flush [:2S :5S :7S :9S :3S])
(def h-straight-6 [:2S :3S :4S :6H :5S])
(def h-straight-5 [:2S :3S :4S :AH :5S])
(def h-straight-T [:8C :9C :7D :TD :6H])
(def h-3-of-a-kind [:2S :5D :2H :2C :3S])

(def h-set-33327 [:3S :3C :2S :7D :3H])
(def h-set-33347 [:3S :3C :4S :7D :3H])
(def h-set-33346 [:3S :3C :4S :6D :3H])
(def h-set-AAA46 [:AS :AC :4S :6D :AH])
(def h-two-pairs55223 [:2S :5D :5H :2C :3S])
(def h-two-pairs5522A [:2S :5D :5H :2C :AS])
(def h-two-pairsAA553 [:AS :5D :5H :AC :3S])
(def h-two-pairsAAKK4 [:AS :KD :KH :AC :4S])
(def h-pair [:2S :5D :7H :2C :3S])
(def h-pair-tt [:2S :TD :7H :TC :3S])
(def h-hi-hand [:2S :5D :7H :8C :3S])
(def h-hi-hand-k-high [:2S :5D :KH :8C :3S])

(def l-hi-hand [:2S :5D :7H])
(def l-hi-hand-a-high [:2S :AD :7H])
(def l-pair-hand [:5S :5D :7H])
(def l-pair-hand-tt [:TS :AD :TH])
(def l-three-of-a-kind-hand [:5S :5D :5H])

(defn- shuffle-converter
  ([fn h1] (hand-keyword-converter fn (shuffle h1)))
  ([fn h1 h2] (hand-keyword-converter fn (shuffle h1) (shuffle h2))))

(deftest test-comparators
  (testing "hi hand comparators"
    (is (= 0 (shuffle-converter high-card-comp h-hi-hand h-hi-hand)))
    (is (= 1 (shuffle-converter high-card-comp h-hi-hand-k-high h-hi-hand)))
    (is (= -1 (shuffle-converter high-card-comp h-hi-hand h-hi-hand-k-high)))
    (is (= 1 (shuffle-converter high-card-comp h-hi-hand-k-high l-hi-hand)))
    (is (= -1 (shuffle-converter high-card-comp h-hi-hand-k-high l-hi-hand-a-high))))
  (testing "pair comparators"
    (is (= 0 (shuffle-converter pair-comp h-pair h-pair)))
    (is (= -1 (shuffle-converter pair-comp h-pair h-pair-tt)))
    (is (= -1 (shuffle-converter pair-comp h-pair l-pair-hand)))
    (is (= 1 (shuffle-converter pair-comp h-pair-tt l-pair-hand)))
    (is (= -1 (shuffle-converter pair-comp h-pair-tt l-pair-hand-tt)))
    (is (= 1 (shuffle-converter pair-comp l-pair-hand-tt h-pair-tt)))
    (is (= 0 (shuffle-converter pair-comp l-pair-hand l-pair-hand))))
  (testing "two pair comparators"
    (is (= 0 (shuffle-converter two-pair-comp h-two-pairs55223 h-two-pairs55223)))
    (is (= -1 (shuffle-converter two-pair-comp h-two-pairs55223 h-two-pairs5522A)))
    (is (= 1 (shuffle-converter two-pair-comp h-two-pairs5522A h-two-pairs55223)))
    (is (= 1 (shuffle-converter two-pair-comp h-two-pairsAA553 h-two-pairs55223)))
    (is (= -1 (shuffle-converter two-pair-comp h-two-pairsAA553 h-two-pairsAAKK4)))
    )
  (testing "set pair comparators"
    (is (= 0 (shuffle-converter set-comp h-set-33327 h-set-33327)))
    (is (= -1 (shuffle-converter set-comp h-set-33327 h-set-33347)))
    (is (= -1 (shuffle-converter set-comp h-set-33327 h-set-AAA46)))
    (is (= 1 (shuffle-converter set-comp h-set-AAA46 h-set-33327)))
    )
  (testing "straight comparators"
    (is (= -1 (shuffle-converter straight-comp h-straight-flush h-straight-T)) "9 T")
    (is (= 1 (shuffle-converter straight-comp h-straight-flush h-straight-6)) "9 6")
    (is (= 0 (shuffle-converter straight-comp h-straight-6 h-straight-6)) "6 6")
    (is (= 1 (shuffle-converter straight-comp h-straight-6 h-straight-5)) "6 5")
    )
  (testing "flush comparators. uses straights as test subjects."
    (is (= -1 (shuffle-converter flush-comp h-straight-flush h-straight-T)) "9 T")
    (is (= 1 (shuffle-converter flush-comp h-straight-flush h-straight-6)) "9 6")
    (is (= 0 (shuffle-converter flush-comp h-straight-6 h-straight-6)) "6 6")
    (is (= -1 (shuffle-converter flush-comp h-straight-6 h-straight-5)) "6 5")
    )

  (testing "full house comparators."
    (is (= 0 (shuffle-converter set-comp h-full-house-999TT h-full-house-999TT)))
    (is (= -1 (shuffle-converter set-comp h-full-house-999TT h-full-house-999QQ)))
    (is (= -1 (shuffle-converter set-comp h-full-house-999TT h-full-house-TTT22)))
    (is (= 1 (shuffle-converter set-comp h-full-house-TTT22 h-full-house-999TT)))
    )

  (testing "four of a kind comparators."
    (is (= 0 (shuffle-converter four-of-kind-comp h-4-of-a-kind-23 h-4-of-a-kind-23)) "same")
    (is (= -1 (shuffle-converter four-of-kind-comp h-4-of-a-kind-23 h-4-of-a-kind-24)) "his differ")
    (is (= 1 (shuffle-converter four-of-kind-comp h-4-of-a-kind-32 h-4-of-a-kind-24)) "his differ")
    (is (= 1 (shuffle-converter four-of-kind-comp h-4-of-a-kind-A2 h-4-of-a-kind-24)) "fours differ")
    (is (= 1 (shuffle-converter four-of-kind-comp h-4-of-a-kind-A2 h-4-of-a-kind-KQ)) "fours differ")
    )
  )


(deftest test-hand-validators
  (testing "straight flush"
    (is (some? (shuffle-converter straight-flush? h-straight-flush))))
  (testing "4 of a kind"
    (is (some? (shuffle-converter four-of-a-kind? h-4-of-a-kind-23))))
  (testing "full house"
    (is (some? (shuffle-converter full-house? h-full-house-999TT))))
  (testing "flush"
    (is (some? (shuffle-converter flush? h-flush))))
  (testing "straight"
    (is (= true (shuffle-converter straight? (shuffle h-straight-T))) "straight 6T")
    (is (= true (shuffle-converter straight? (shuffle h-straight-5))) "straight a5")
    (is (= true (shuffle-converter straight? (shuffle h-straight-6))) "straight 26")
    )
  (testing "3 of a kind"
    (is (some? (shuffle-converter three-of-a-kind? h-3-of-a-kind))))
  (testing "two pair"
    (is (= true (shuffle-converter two-pairs? h-two-pairsAAKK4))))
  (testing "pair"
    (is (some? (shuffle-converter pair? h-pair))))
  (testing "hi hand"
    (is (= true (shuffle-converter high-card? h-hi-hand)))))

(deftest test-low-hands
  (testing "low hand hi"
    (is (= true (shuffle-converter high-card? l-hi-hand))))
  (testing "low hand pair"
    (is (not (some? (shuffle-converter pair? l-hi-hand))))
    (is (some? (shuffle-converter pair? l-pair-hand))))
  (testing "low hand 3 of a kidn"
    (is (not (some? (shuffle-converter three-of-a-kind? l-hi-hand))))
    (is (not (some? (shuffle-converter three-of-a-kind? l-pair-hand))))
    (is (some? (shuffle-converter three-of-a-kind? l-three-of-a-kind-hand)))))

(deftest test-value
  (testing "straight flush"
    (is (= 8 (value h-straight-flush)))))

;; todo generative testing would work here nicely.
