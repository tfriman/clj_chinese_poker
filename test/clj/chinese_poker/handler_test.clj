(ns chinese-poker.handler-test
  (:require
   [clojure.test :refer :all]
   [ring.mock.request :refer :all]
   [chinese-poker.handler :refer :all]
   [mount.core :as mount]))
(use-fixtures
  :once
  (fn [f]
    (mount/start #'chinese-poker.config/env
                 #'chinese-poker.handler/app-routes)
    (f)))

(deftest test-app
  (testing "main route"
    (let [response ((app) (request :get "/"))]
      (is (= 200 (:status response)))))

  (testing "not-found route"
    (let [response ((app) (request :get "/invalid"))]
      (is (= 404 (:status response))))))
