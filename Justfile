dotenvfile := ".env"

default:
  @just --choose

@list:
  just --list

alias be := backend

mongo:
  docker run -d -p 27017:27017 --name poker-mongo \
    -v mongo-data:/data/db \
    -e MONGODB_ROOT_USERNAME=poker-user \
    -e MONGODB_ROOT_PASSWORD=hiluhei \
    -e MONGODB_AUTH=true \
    webhippie/mongodb:5-arm64

backend:
  #!/usr/bin/env bash
  set -o allexport
  source {{dotenvfile}}
  set +o allexport
  lein run

alias fe := frontend

frontend:
  shadow-cljs watch app

lint:
  lein check

test:
  lein test | tee test_out_gitignore.log

package:
  shadow-cljs release app && lein uberjar

package-old:
  lein uberjar

clean:
  lein clean

setupshadowjs:
  yarn global add shadow-cljs

setupnpm:
  npm install

update-deps:
  lein ancient upgrade

update-clojure:
  lein ancient upgrade :check-clojure
