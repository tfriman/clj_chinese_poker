(ns ^:dev/once chinese-poker.app
  (:require
    [chinese-poker.core :as core]
    [cljs.spec.alpha :as s]
    [expound.alpha :as expound]
    [devtools.core :as devtools]))

(extend-protocol IPrintWithWriter
  js/Symbol
  (-pr-writer [sym writer _]
    (-write writer (str "\"" (str sym) "\""))))

(set! s/*explain-out* expound/printer)

(enable-console-print!)

(devtools/install!)

(prn "chinese-poker.app calling init starting in dev")
(core/init)
(prn "chinese-poker.app calling init done in dev")
