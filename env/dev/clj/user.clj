(ns user
  "Userspace functions you can run by default in your local REPL."
  (:require
    [chinese-poker.config :refer [env]]
    [chinese-poker.core]
    [clojure.pprint]
    [clojure.spec.alpha :as s]
    [expound.alpha :as expound]
    [mount.core :as mount]
    [shadow.cljs.devtools.api :as shadow-api]
    [shadow.cljs.devtools.server :as shadow-server]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(add-tap (bound-fn* clojure.pprint/pprint))

(defn start
  "Starts application.
  You'll usually want to run this on startup."
  []
  (-> (mount/with-args {:dev        true
                        :port       3000
                        :nrepl-port nil
                        })
      (mount/start-without #'chinese-poker.core/repl-server)))

(defn stop
  "Stops application."
  []
  (mount/stop-except #'chinese-poker.core/repl-server))

(def lastdecknum (atom 0))

(defn- fixed-deck-fn
  "Return fixed games from file, loops them"
  []
  (let [c (atom -1)
        decks (read-string (slurp "env/dev/clj/fixed-deck.edn"))]
    (fn []
      (let [decknum (mod (swap! c inc) (count decks))]
        (println "sending deck:" decknum)
        (reset! lastdecknum decknum)
        (nth decks decknum)))))

(comment
  ;; generate random deck
  (count (read-string (slurp "env/dev/clj/fixed-deck.edn")))
  )

(def fixed-game {:start (fn []
                          (chinese-poker.engine/create-game-fn (fixed-deck-fn))
                          )})

(defn start-with-fixed-create-game
  "Starts game with known games."
  []
  (-> (mount/with-args {:dev        true
                        :port       3000
                        :nrepl-port nil
                        })
      (mount/only [#'chinese-poker.config/env
                   #'chinese-poker.handler/init-app
                   #'chinese-poker.engine/create-game
                   #'chinese-poker.handler/app-routes
                   #'chinese-poker.core/http-server])

      (mount/swap-states {#'chinese-poker.engine/create-game fixed-game})
      mount/start))

(defn fixed-game-restart
  "restart with fixed deck"
  []
  (stop)
  (start-with-fixed-create-game))

(comment
  (fgrestart)
  )

(defn restart
  "Restarts application."
  []
  (stop)
  (start))

(defn reset-persistence!
  "Reset in-memory persistence, tournaments and games"
  []
  (reset! chinese-poker.persistence.persistence/a-tournaments {})
  (reset! chinese-poker.persistence.persistence/a-games {}))


(defn fe-start-watch []
  (shadow-server/start!)
  (shadow-api/watch :app)

  )

(defn fe-switch-runtime []
  ;; TODO add users.cljs or something alike to switch browsers https://github.com/thheller/shadow-cljs/discussions/1019
  )

(defn start-all []
  (start)
  (fe-start-watch))