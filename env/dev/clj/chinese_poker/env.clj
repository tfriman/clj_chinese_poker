(ns chinese-poker.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [chinese-poker.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[chinese-poker started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[chinese-poker has shut down successfully]=-"))
   :middleware wrap-dev})
