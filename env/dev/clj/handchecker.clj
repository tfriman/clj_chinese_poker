(ns handchecker)

;; HELPER. This is for testing only, checks if those fixed decks match.

(def decks (read-string (slurp "env/dev/clj/fixed-deck.edn")))

(defn hand->map [handraw]
  (let [hand (map :display handraw)
        u (sort (take 5 hand))
        m (sort (take 5 (drop 5 hand)))
        l (sort (take-last 3 hand))
        lu (take-last 3 hand)]
    {:u u
     :m m
     :l l
     :lu lu}
    ))

(defn prn-hands [deck]
  (let [h1 (hand->map (take 13 deck))
        h2 (hand->map (take 13 (drop 13 deck)))]
    (map (fn [kw] [(kw h1) (kw h2)]) [:u :m :l])
    ))

(defn prn-hands-same [deck reverse?]
  (let [h1 (hand->map (take 13 deck))
        h2 (hand->map (take 13 (drop 13 deck)))
        pf (if reverse? reverse identity)]
    (mapv (fn [kw] (pf [(kw h1) (kw h2)])) [:u :m :l :lu])))

(defn all-hands-comp [d]
  (for [i (range (count d))]
    (do
      (prn-hands-same (nth d i) (even? i)))))

(comment
  (def ah (all-hands-comp decks))
  ;; h0
  [(("2C" "2S" "5D" "7S" "QS") ("3S" "8H" "JS" "QH" "TH"))
   (("3D" "4C" "4H" "6D" "KS") ("7C" "8S" "9D" "9S" "AD"))
   (("5H" "AC" "TD") ("2H" "9H" "AH"))]
  (def h0 {})
  ;; h1
  [[("2C" "7H" "7S" "8H" "KD") ("8C" "8D" "JC" "QC" "TS")]
   [("2H" "3D" "6D" "AD" "TD") ("4C" "4S" "JH" "KS" "TC")]
   [("3C" "5C" "AH") ("6S" "9H" "QD")]]
  ;; p1 1 p2 2 ;; CORRECT p2 foobot
  ;; XXXXX engine vaittaa etta p1 voitti kaikki!
  (def h1 {:p1 1 :p2 2})
  ;; h2
  [(("4S" "5S" "9H" "JC" "JS") ("2D" "2H" "6D" "6H" "JD"))
   (("AC" "KH" "QH" "QS" "TH") ("2C" "2S" "5D" "JH" "TC"))
   (("6C" "KC" "KS") ("3D" "3H" "AD"))]
  ;; bust
  (def h2 {})
  ;; h3
  [[("3D" "5H" "QS" "TH" "TS") ("2H" "4H" "5S" "8S" "JD")]
   [("3H" "4C" "AC" "AD" "KS") ("5C" "8C" "9C" "9S" "JS")]
   [("2S" "KC" "QH") ("2D" "8D" "AS")]]
  ;; bust
  (def h3 {})
  ;; h4
  [(("6H" "7D" "8H" "AC" "KC") ("3C" "5D" "6C" "KH" "QD"))
   (("2H" "6D" "QC" "TC" "TH") ("4C" "4H" "7H" "8D" "AD"))
   (("2C" "3D" "7S") ("2D" "9S" "QS"))]
  ;; bust
  (def h4 {})
  ;; h5
  [[("5D" "6S" "7H" "8S" "TS") ("2S" "5S" "7C" "9S" "AS")]
   [("2D" "2H" "3C" "6C" "6H") ("AC" "KS" "QC" "QH" "TC")]
   [("4H" "7S" "QD") ("4S" "5C" "KH")]]
  ;; bust
  (def h5 {})
  ;; h6 (nth (all-hands-comp decks) 6)
  [(("4H" "5S" "KD" "QS" "TC") ("4D" "6S" "7D" "8C" "JS"))
   (("2C" "3C" "4C" "8S" "AS") ("2H" "5H" "9H" "KH" "KS"))
   (("5D" "JC" "QH") ("4S" "AD" "AH"))]
  ;; bust
  (def h6 {})
  ;; h7
  [[("2C" "2S" "5D" "5H" "AH") ("4C" "4H" "4S" "6D" "QD")]
   [("3D" "5C" "8H" "AS" "JC") ("7H" "AC" "JS" "QH" "TD")]
   [("3S" "4D" "8S") ("5S" "AD" "JH")]]
  (def h7 {:p2 3}) ;; CORRECT p2 foobot
  ;; h8
  [(("5S" "8S" "9H" "TH" "TS") ("3C" "5D" "8H" "QC" "QH"))
   (("6H" "7C" "AC" "AS" "KD") ("2D" "5C" "6S" "9S" "AH"))
   (("6D" "QS" "TD") ("3D" "9D" "JH"))]
  (def h8 {:p2 3});; CORRECT  p2 foobot
  ;; h9
  [[("2H" "3H" "4S" "6H" "TC") ("7C" "7D" "8S" "AD" "QH")]
   [("6C" "9C" "9S" "KS" "QS") ("2S" "4D" "6D" "8H" "AS")]
   [("4H" "8D" "9H") ("2D" "3D" "7S")]]
  (def h9 {:p2 3}) ;; CORRECT p2 foobot

  (merge-with + h0 h1 h2 h3 h4 h5 h6 h7 h8 h9 )
  {:p1 1, :p2 11}
  ;; get last cards for each hand.
    (map (fn [d] [(:display (nth d 12)) (:display (nth d 25))]) decks)
  )
