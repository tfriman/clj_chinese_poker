(ns chinese-poker.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[chinese-poker started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[chinese-poker has shut down successfully]=-"))
   :middleware identity})
