(ns chinese-poker.app
  (:require [chinese-poker.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init)
