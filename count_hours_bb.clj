#! bb

;; Counts hours from hours.org file.

(->> *command-line-args*
     first
     slurp
     clojure.string/split-lines
     (map (fn [l]
            (-> (re-find #".*>(?:[ ]*)([\.|\d]+)(?:[ ]*)h.*" l)
                second
                ((fnil clojure.string/trim "0"))
                Double/parseDouble
                )))
     (reduce + 0)
     (println "Total hours:" ))

(comment
  ;; This contains more verbose way of doing this but also logs lines that are not parseable
  (def inputs (-> *command-line-args*
                  first
                  slurp
                  clojure.string/split-lines))

  (println "Lines:" (count inputs))

  (defn parse [l]
    (let [[_ x & _] (re-find #".*>(?:[ ]*)([\.|\d]+)(?:[ ]*)h.*" l)]
      (try
        (-> (clojure.string/trim x)
            Double/parseDouble)
        (catch Exception e
          (println "failed: " l " and x " x)
          0))))

  (comment
    (println (parse "<2019-11-03 Su> 2h restart with websocket base"))
    (println (parse "<2019-11-03 Su> 2.5h restart with websocket base"))
    (println (parse "<2019-11-03 Su> 3.5 h restart with websocket base"))
    (println (parse "<2019-11-03 Su> 4 h restart with websocket base"))
    (println (parse "<2019-11-03 Su>12.5h restart with websocket base")))

  (println "Total hours: " (reduce + 0 (map parse inputs))))