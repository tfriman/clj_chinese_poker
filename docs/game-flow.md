# Here be game logic.

# UI Game Flow with BE actions needed

## Login

## Create tournament (player1)
- BE store it
- BE distribute it to possible joiners?

## Join tournament (player2-4)

- BE store data
- BE prevent double join
- BE prevent joining to game which does not accept new player (i.e. has started already)
- FE show visually to player1 that there is a new player

## Start tournament (player1)

- BE change tournament state to started
- BE prevent double start?
- BE inform tournament players that status changed

## Start dealing (player1)

Is this step needed or should this be automatic when tournament is started?

- BE create a new game for tournament

## Play the given game (all players in tournament)

- BE handle rejoins etc
- BE store results to tournament
- FE any player can press pause and next game won't be autodealed.

## Stop the tournament (player1)

- BE store status

## Restart the tournament (player1)

When? Notification that all players are available?

## Other things

- BE player can be active only in one tournament at time
- FE show the active tournament
- FE show all joined tournaments
- FE show search for tournaments
