# Security

[Keycloak](https://keycloak.org) is used as a security product. It will hide the other possible authentication providers like Github, Google and other OIDC providers.

All should be running under TLS, meaning https and wss for Websockets

## Development setup

Start local Keycloak (v22+). Create a realm for this (possibly named "chinese"). Copy keycloak.json from the UI and store it to 
env/dev/resources. Create 2 test users in that schema. Try to login to http://localhost:3000 with those.

TODO make keycloak configurable in both FE and BE, currently in code references to http://localhost:8080.

## Backend REST endpoints security
TO BE DONE, only websocket checks now the token and that is not bulletproof because a) there is no test and b) socket is not closed if auth fails.

JWT Bearer token is checked against Keycloak's well known OIDC configuration. Issuer and Realm are checked in addition to token expiration and signature validity.

## Websocket security

My amateur assessment is that this is pretty far from production ready deployment for secure Websocket communication setup.

Websockets provide no out of the box functionality to authenticate during the handshake. Thus JWT Bearer token is used for that.
Open channel is connected with the identity.

TODO: Content Security Policy (CSP): Use CSP headers to restrict sources of data, mitigating XSS attacks.

TODO: X-Frame-Options: Prevent clickjacking by ensuring your web application is not embedded into potentially malicious sites.

TODO: Enforce periodic re-authentication with JWT 

TODO: add rate limiting and throttling to the Websocket connections and requests

TODO: add origin header check when Websocket handshake is initialized.

## JWT refresh

TODO enforce periodic refreshes. Timeout for the JWT is checked.