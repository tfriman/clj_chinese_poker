# Study about Finite state machines and frontend (FE) events.

## Status

WIP. Maybe the option [Go with the existing](#l-existing) for now

I have to wrap my head around where FSM makes sense in this. I feel
the examples I've browsed through are too simplistic, like login view
state machine. And then again, there are obviously different needs
from the point of UI interactions (if not logged in, show login
screen, if there is an ongoing tournament, show it... etc) and those
related to the game, like "can I put this card to the given hand" and
"can I submit". On the BE side there should be a state machine
accepting given game events. Also, it is hard to distinguish which UI
actions should change state, if they change state, should that be
relayed to the BE too etc. In other word, which UI events cause
transition in the FSM?

## FSM ideas

"In addition to a centralized abstraction which describes our control flow, this design adds flexibility, making it easier for us to modify and extend later." from [https://cognitect.com/blog/2017/5/22/restate-your-ui-using-state-machines-to-simplify-user-interface-development](https://cognitect.com/blog/2017/5/22/restate-your-ui-using-state-machines-to-simplify-user-interface-development)

From: [https://cognitect.com/blog/2017/8/14/restate-your-ui-creating-a-user-interface-with-re-frame-and-state-machines](https://cognitect.com/blog/2017/8/14/restate-your-ui-creating-a-user-interface-with-re-frame-and-state-machines)

We've added two new subscriptions: [:failure] and
[:login-disabled]. This is another spot where this approach shines,
because we're able to implement several subscriptions soley based on a
single piece of data -- our state machine's current state. We don't
need to complicate app state with multiple attributes to keep track of
[:failure] and [:login-disabled], because we're able to derive their
values from :state.

...

Again, we make use of our existing utility function to advance the
state machine. Why didn't we dispatch an additional event in this
case? The [:change-email] event is already represented in our state
machine, and there isn't any conditional logic in the handler --
there's no decision to trace.

Debugging state machines in the FE side:

Github stuff: [https://github.com/jebberjeb/reframe-fsm/tree/master/login-fsm](https://github.com/jebberjeb/reframe-fsm/tree/master/login-fsm)

From: [src/cljs/reframe_fsm/core.cljs](https://github.com/jebberjeb/reframe-fsm/blob/master/login-fsm/src/cljs/reframe_fsm/core.cljs)
``` clojure
(def debug (rf/after (fn [db event]
                       (.log js/console "=======")
                       (.log js/console "state: " (str (:state db)))
                       (.log js/console "event: " (str event)))))

(def interceptors [debug])

```


### Own thinking

State machine as such is stateless so you should store the state
machines state to somewhere, like in RAtom in the FE or in the DB in
the BE.

### Tips from [https://cognitect.com/blog/2017/8/14/restate-your-ui-creating-a-user-interface-with-re-frame-and-state-machines](https://cognitect.com/blog/2017/8/14/restate-your-ui-creating-a-user-interface-with-re-frame-and-state-machines)

TIP #1 Your state machine shouldn't require more than a page of Clojure code.

Your first goal with this approach is building a useful model --
something that describes your UI at a glance. Beyond this size, you
should consider splitting it up into multiple state
machines. Generating a diagram from your state machine's data can help
with this, but usually right around the time the code becomes hard to
read, so does the diagram.

TIP #2 Keep similar things together.

You don't need to create one state machine for your entire UI, or even
a single view. For example, if your UI has a user profile page which
lets the user update their mailing addres in one pane, and
notifications settings in another, you'll probably want to separate
your state machines similarly. We'll go into more depth on this in a
future post.

TIP #3 Not all Re-frame events will be FSM transitions.

You probably won't have a transition in your state machine for every
Re-frame event in your UI. That's ok. For example, a button click
event might not have a transition in your state machine, but its event
handling function could contain conditional logic to perform a
:submit-form transition if form validation succeeds.

## Visualization

[https://cognitect.com/blog/2017/5/22/restate-your-ui-using-state-machines-to-simplify-user-interface-development](https://cognitect.com/blog/2017/5/22/restate-your-ui-using-state-machines-to-simplify-user-interface-development)
``` clojure
(require 'fsmviz.core)
 (fsmviz.core/generate-image {'Ready {:missing-password ...} ...}
                                   "fsmui.png")
```

## Goal
Study goal: Does it make sense to have a finite state machine to orchestrate events at least on the FE side?

Problem can be described like this: originally the FE design with
events received from server and FE create an implicit state on the UI
side (and also on the backend(BE) side, but somehow that is more
manageable, my guess is I have done more backend stuff and also BE
interactions are somewhat clearer to follow for some reason). I faced
this implicit state management problem in a harsh way when I tried to
decipher what needs to be done to get the ongoing game rendered to the
screen after player reconnects.

Seems this is a very common problem, original idea of using events was
clear and quite sudden code has interdependencies and state is not
manageable any longer. This modeling is called bottom-up approach [1](#fn1).
Instead, event-state-action paradigm could describe all the states and
events and describe the actions based on those. I think making the
states and their transitions clear helps a lot also when I restart
coding (like happened now after 11 months, it took again ages to
figure out what happens and why).

In order to keep the goal in sight I need to weigh the cost of
introducing the state management now and the future development
efforts if I don't. If I go down to the rabbit hole with finite state
machines (FSM) I need to model the current implicit state first and
that is not a light chore. Then I need to consider if I should and
could use the same FSM on both FE and BE or should they just share
some parts? On the other hand, I think I could hack this reconnect
stuff by sending correct events in the FE side when reconnect has
happened. That would make this ever lasting project progress a lot
towards the state (pun intended) where it could be considered version
1.0. Also [2](#fn2) points out the ageless wisdom of not introducing
additional complexity (FSM etc) unless the timing is right meaning not
too soon and not too late. I would lean on this project definitely not
being too soon in any ways one can understand soon so that checkpoint
has been passed for sure. Is it too late, maybe, but I'm not a
clairvoyant so better late than never.

## Current issues in plain English

How does the FE restore a given game so that players can continue?

## FSM ideas and modeling principles applicable to this project

Create a state transition matrix!

- figure out what are the main states and substates.

- Explicitly document what is possible in each state.
- Manage the state transitions in a single place.
- Possibly see the events needed to reach a given state

Should there be more than one state machines? For example, tournament could have one and each game their own?

Should there be an event store which contains all the events fed to
the state machine or how does the state reconstruction happen? Could
server just send the state machine representation of the ongoing game
back to the client and can client use that to reconstruct the state?

See https://github.com/jarppe/fsm/blob/main/src/examples/examples/quick.cljc
```clojure
(-> (reduce fsm/apply-signal
            fsm
            ["ping" "ping" "ping"])
    :state)
```

So how about each fsm has also events fed to it too? There would be
:fx added to each appropriate scenario and those can then be used to
reconstruct the FE state? Con is that this ties the FE stuff to the
FSM events but is that somehow avoidable?

What is the easiest use case to try this out? Should I create a
"shadow" FSM on the BE side and also send that?

There are at least two different players and machines? How does the
reconstruction work here? BE sees both players' events and should it
just send them to each? Then the events should have information about
the player making the move and updating accordingly?

BE side event capture could go like this: get event, get game fsm,
feed the event there to see that is ok, get the action back and act
accordingly. Does this mean that the actual FSM should contain the
moves too? Could this be as simple as that, store the feed of the
events to a game and then replay them in the FE? Managing those events
could be done using FSM but that is not technically mandatory. FE side
should then know which player is acting and place the cards
accordingly for each player?

## Alternatives without FSM

I think I have not fully grasped the sweet spot of FSM in this
domain. Game logic (allowed moves) is a one possible place where FSM
would be used but that would be in the BE side mainly, right? In the
FE side there are components (like view for selecting cards to place,
hand selector for card, which could benefit from the FSM. But I think
introducing FSM for managing all the state in the FE would be a very
demanding task.

### Store events to an unfinished game
Just store events to each game. If game is not finished, send those events to the FE.

Pros: easy to do. Provides a mean to replay hand and possibly undo too.

Cons: different handling paths in the FE side for restoration?

### <a id="l-existing"/>Go with the existing, just store the state in the login event.

Pros: fastest to do.

Cons: point solution. Does not help to understand how FE events interact with each other and state.

## Material

<a id="fn1"/> [1 Tackling UI complexity with State Machines](https://medium.com/@carloslfu/tackling-ui-complexity-with-state-machines-b3f1eb6d1a97)
One take-away: "reactive systems are complex because of event orchestration complexity"

<a id="fn2"/> [2 http://raganwald.com/2018/02/23/forde.html](http://raganwald.com/2018/02/23/forde.html)
“begin with the end in mind.”

<a id="fn3"/> message dispatcher [3 https://github.com/metosin/viesti/blob/develop/test/viesti/core_test.cljc](https://github.com/metosin/viesti/blob/develop/test/viesti/core_test.cljc)

<a id="fn4"/>  [4 clj/s fsm but not in clojars](https://github.com/jarppe/fsm)