# Design principles, decisions and guidelines

This document describes guiding principles behind this project as I now them recall.

## First make it work, then make it beautiful and then make it fast.

We are barely in the first phase but this principle is still
solid. There are unnecessary complexities in some places I know of and
probably in many I don't.

## Have barely enough tests, emphasis on end to end tests instead of unit tests

Having test suite that finds majority (90%) of the regressions is a
must. I've spent a lot of time trying to create automated game play
with two players to make sure it still works.

Browser testing is completely missing at this point. Reason is that
I've lacked both time and knowledge how to do it so I decided to trust
on backend testing mainly.

## Embrace dynamic way and try to manage without type checking

This is not based on anything else except a trial how far one can get
without adding schema checks etc. Dynamic way is fast to write but I
guess we are approaching the limit pretty soon. I've done so many
rewrites during this project that I feel this decision was a good one
but maybe design is now stable enough for adding some checks.

## YAGNI you aren't gonna need it

This is a newer one. Goal with this is to keep the complexity at bay.
Adding too much right in the beginning seems to be
counterproductive. Countless times I've forgotten I've added a hook or
function for extension and never actually implemented that.

# Accidental decisions and learnings

## Learn by doing

Style and design wise this project is not a coherent one. I started
this without proper knowledge how to create a multiplayer game
implemented as a single page app using websockets. Not to mention my
lack of knowledge around ClojureScript and React. Also my Clojure
skills were on the very beginner level.

## Prioritize done over good

I have toyed with the thought of starting from the scratch but I've
done that already few times. Rewrites can be fun but I rarely had time
to dedicate this so each rewrite just pushed the finishing line
further thus reducing motivation. When I restarted this autumn 2021 I
chose the path of living with my past sins as far as possible and
doing rewrites or redesigns only when there are clear benefits of
doing so.

## Goals can contradict

Originally I started this project to learn Clojure/Script and single
page app stuff. That goal is more or less completed. Another goal was
to make something useful for myself and my friends. These seemingly
sensible goals actually were contradicting pretty heavily.

First of all see previous "learn by doing" and "done over good"
sections. I have learned a lot but I've failed badly with the second
goal. Trying things out while you are learning probably should have
been the single goal. Now I've felt I have failed with this project
because this is still under construction.
