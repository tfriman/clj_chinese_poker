# rename events
:start-game is double used on client side!

## ideas:
naming: originator-verb-subject?
:client-start-game

or :client-ready-for-game
or :command-start-game and :event-game-started ?

# Add test for both busting, 0 points
# player turn rotation is randomized. should it be rotating regularly?
# Bugs
## after game ends, start game button does not work
2021-05-03 22:33:32,532 [worker-1] INFO  chinese-poker.routes.websockets - handle-channel-recv!  {:user :brave, :type :create-game, :tournament :c2da5d7b-2ade-46e6-9ad7-ad5c6bed393b}
Mon May 03 22:33:32 EEST 2021 [worker-1] ERROR - handle websocket frame org.httpkit.server.Frame$TextFrame@25bef87
java.lang.IllegalArgumentException: No method in multimethod 'handle-msg!' for dispatch value: :create-game

# Tasks
## replace all stuff with keywords with ws traffic
to make repl interactions easier.
## result format
:winners {:u [:p2], :m [:p2], :l [:p2]}, :points {:u 1, :m 1, :l 1}}

most usable: :player-points {:p1 2 :p2 1 :p3 0}

## Create test case for counting tournament total points?
## Figwheel main

See files:

- figwheel-main.edn
- dev.deps.edn

# Plan
## things to test
### re-frame-10x
https://github.com/day8/re-frame-10x

## Tournament start and join instead of game.
Flow:
1) create tournament
2) join tournament
3) start tournament
4) start game
5) end game
6) loop 4&5
7) pause / complete tournament

Show online status of players

Show accumulated points

Select point calc rules in the tournament creation.

## TODO

### websocket disconnect, how to notice?

https://github.com/http-kit/http-kit/issues/111

### send available tournaments when logged in

### frontend: make ws sends an effect via re-frame

### Unit test game logic

With websocket client? https://github.com/stalefruits/gniazdo

Preferably just the logic without websocket layer but how?

https://stackoverflow.com/questions/4361173/http-headers-in-websockets-client-api

test flow for websocket stuff.
on backend side:
- inject game engine result for tournament

- after game,check results

ws side
create two wsockets
- run game

mocking the engine, options:
using alter-var-root https://github.com/tgk/pet-shop-example/blob/master/test/integration_test.clj
https://clojuredocs.org/clojure.core/alter-var-root

use the mount to override

https://github.com/tolitius/mount

deck could be overridden? shuffle is a problematic one, needs to be moved from engine

### TODO remove luminus http and unify mount start and config
https://github.com/luminus-framework/luminus-http-kit/blob/master/src/luminus/http_server.clj

https://github.com/luminus-framework/examples/blob/master/multi-client-ws-http-kit/project.clj

### create hand evaluator
- valid?
- who won
+ possibility to have bonus points and change rules

## Imaginary game flow:
- player1 registers (once)
- player2 registers (once)
- player1 creates a game g1
- player2 joins g1
- g1 starts (not an event)
- both are given 5 cards, p2 has to set all first
- p2 submits his cards 1-5 to server
- server submits those changes to p1 and possibly sends p2 card #6
- p1 receives cards p2 set to three hands
- p1 places his 5 cards and submits it to the server
- server submits those changes to p2
- server submits card #6 to p2
- p2 places card6 and submits to the server
- server sends p2's ALL card sets to p1
- server sends p1 her card #6
... this goes on until p1 has set her c#13
- server evaluates hands
- server checks winners hand by hand
- server optionally applies bonus rules
- server stores result to database
- server sends info about hand result to both p1 and p2
- possibly a new game starts

Scenarios not handled here:
- game is paused and rejoined afterwards
- how players find each other

## Guiding principles

* Have fun!
* MVP >> optimum / perfect solution grinding ad infinum
* some new, some old but no all new
* Clojure all the way
* mobile browser ui first
* 2 way game first
* Skype or something alike for communication, no in-game with MVP

## Ideas
* pact
* spec

No:
* datomic
* CQRS
