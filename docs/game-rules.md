# Game rules and instructions

Chinese poker is a game for 2-4 players. 13 cards will ultimately be
placed by each participant. Game boards consist of 3 hands: upper (5
cards), middle (5 cards) and lower (3 cards).

Boards need to be valid to be counted. Validity rule is pretty simple
one: upper hand needs to be better than middle hand (using regular 5
card draw hand ranking rules) and middle needs to be better than lower
hand.

All valid boards are compared hand by hand and the winner of the hand
gets point from win. If one of the players wins all the hands (for
example if only one player has a valid board) she "scoops" and gets 6
points (3 additional bonus points).

## Game flow for half-open Chinese Poker

This variant of Chinese poker is called half-open (I hope that is the
correct term!). Game starts with 5 cards dealt to each of the
participants. First player to action places all 5 cards to
upper/middle/lower as she pleases. Then the next player does the same
until all players have placed their 5 cards.

Game then continues card by card, each player gets a card to be placed
on available card slots in upper/middle/lower.

When all hands have all cards, round is ended and scored. Next hand
begins after that.

## Very basic strategy advice

As with poker usually this is a game of probabilities. Your
information about the possible outs (good cards helping you) and busts
(bad cards ruining your play) grows as there are more visible cards.

Placing those 5 as a first player is the hardest task because there is
no additional information. Thus you should try to place your cards so
that you maximize the odds of having a valid board. Preferably don't
place a pair on the lower at this place.

If there are 4 players you can count in the last placing rounds the
cards that are actually left on the deck and optimize your strategy
accordingly.

If you are leading, try to optimize for keeping your board valid. If
you see you are losing all hands, try to optimize (even with a long
shots) for winning at least one hand.

As stated, these are very basic strategy advice and you probably need
to adjust.

## Bonus rules

There are other possible bonus rules than scooping rule too. Hopefully
those will be added to game later on.
