(ns chinese-poker.subs
  (:require
    [com.rpl.specter :as specter]
    [day8.re-frame.http-fx]
    [re-frame.core :as rf]
    ))

(defn- is-key-fn?
  "TODO refactor me, there is a duplicate"
  [k]
  (fn [m] (= k m)))

;;subscriptions
(rf/reg-sub
  :last-result
  (fn [db _]
    (:last-result db)))

(rf/reg-sub
  :game-ended
  (fn [db _]
    (:game-ended db)))

(rf/reg-sub
  :winners
  (fn [db _]
    (:winners db)))

(rf/reg-sub
  :game-started
  (fn [db _]
    (:game-started db)))

(rf/reg-sub
  :logged-in?
  (fn [db _]
    (keyword? (:user db))))

(rf/reg-sub
  :players
  (fn [db _]
    (:players db)))

(rf/reg-sub
  :other-players
  :<- [:user]
  :<- [:players]
  (fn [[user players] _]
    (filter #(not= (keyword user) (second %)) players)))

(rf/reg-sub
  :this-player
  :<- [:user]
  :<- [:players]
  (fn [[user players] _]
    (first (filter #(= (keyword user) (second %)) players))))

(rf/reg-sub
  :ready-to-send?
  :<- [:placed-cards]
  :<- [:cards-to-place-for-player]
  (fn [[a b] _]
    (let [ac (count a)
          bc (count b)]
      (and (pos? ac) (= ac bc)))))

(rf/reg-sub
  :placed-cards
  (fn [db _]
    (:placed-cards db)))

(rf/reg-sub
  :cards-to-place
  (fn [db _]
    (:place-cards db)))

(rf/reg-sub
  :cards-to-place-for-player
  :<- [:user]
  :<- [:players]
  :<- [:cards-to-place]
  (fn [[user players cards2place] _]
    ;; check if next player is ME!
    (if (= ((or (:next cards2place) ::notfound) players) (keyword user))
      (:cards cards2place)
      {})))

(rf/reg-sub
  :moves
  :<- [:cards-to-place]
  (fn [cards2place _]
    (:moves cards2place)))

(rf/reg-sub
  :game-id
  :<- [:cards-to-place]
  (fn [cards2place _]
    (:game-id cards2place)))

(rf/reg-sub
  :is-street-filled?
  :<- [:cards-to-place]
  :<- [:this-player]
  (fn [[cardsplaced [pid _]] [_ street]]
    (let [moves (:moves cardsplaced)
          result (get-in moves [pid street :cards])
          max (street {:u 5 :m 5 :l 3})
          filled? (= max (count result))]
      filled?
      )))

(rf/reg-sub
  :ongoing-tournaments-game-id
  :<- [:on-going-tournaments]
  (fn [tournaments [_ tournament-id]]
    (-> (filter #(= tournament-id (:id %)) tournaments)
        first
        :ongoing-game-id)))

(rf/reg-sub
  :game-messages
  (fn [db _]
    (:game-messages db)))

(rf/reg-sub
  :tournaments
  (fn [db _]
    (:tournaments db)))

(rf/reg-sub
  :on-going-tournaments
  (fn [db _]
    (:on-going-tournaments db)))

(rf/reg-sub
  :active-tournament
  (fn [db _]
    (:active-tournament db)))


(rf/reg-sub
  :tournament-players
  (fn [db [_ tournament-id]]
    (doto
      (specter/select [:tournaments specter/ALL (specter/if-path [:id (is-key-fn? tournament-id)] [:players specter/ALL])] db)
      ((fn [players] (println "Tournament-players for" tournament-id "are" players))))))

(rf/reg-sub
  ;; TODO this duplicates code a lot compared to :tournament-players reg-sub
  :ongoing-tournament-players
  (fn [db [_ tournament-id]]
    (doto
      (specter/select [:on-going-tournaments specter/ALL (specter/if-path [:id (is-key-fn? tournament-id)] [:players specter/ALL])] db)
      ((fn [players] (println "Ongoing tournament-players for" tournament-id "are" players))))))

(rf/reg-sub
  :new-game-messages
  :<- [:user]
  :<- [:game-messages]
  (fn [[user ge] _]
    (println "new game msg called for user " user " and games:" ge)
    (take 2 (filter #(and (= :game-created (:type %)) (not= user (:creator %))) ge))))

(rf/reg-sub
  :own-games
  :<- [:user]
  :<- [:game-messages]
  (fn [[user ge] _]
    (println "own games called for user:" user " and games:" ge)
    (take 2 (filter #(= user (:creator %)) ge))))

(rf/reg-sub
  :messages
  (fn [db _]
    (take 10 (:messages db))))

(rf/reg-sub
  :user
  (fn [db _]
    (:user db)))

(rf/reg-sub
  :page
  (fn [db _]
    (:page db)))

(rf/reg-sub
  :common/error
  (fn [db _]
    (:common/error db)))
