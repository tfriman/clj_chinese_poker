(ns chinese-poker.events
  (:require
    [ajax.core :as ajax]
    [chinese-poker.websockets :as ws]
    [chinese-poker.serialization-helper :as json-tools]
    [com.rpl.specter :as specter]
    [day8.re-frame.http-fx]
    [re-frame.core :as rf]
    [vimsical.re-frame.cofx.inject :as inject]))
;; Effects
;; TODO reorganize these by grouping
(defn- only-config
  "Reset db to have only :config key"
  [db]
  (select-keys db [:config]))

(defn- is-key-fn?
  "TODO refactor me, there is a duplicate"
  [k]
  (fn [m] (= k m)))

(rf/reg-fx
  :fe-websocket-send
  (fn [msg]
    (println "FX :fe-websocket-send for msg:" msg)
    (ws/send-msg! msg)
    {}                                                      ;; this should be ok to return from effect handler.
    ))

(rf/reg-event-db :fe-initialize-db (fn [db _] (or db {:page :login})))

(rf/reg-event-fx
  :fe-fetch-config
  (fn
    [{db :db} [_ _]]
    {:http-xhrio {:method          :get
                  :uri             "/spaconfig.json"
                  :timeout         2000
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [:fe-process-config]
                  :on-failure      [:fe-handle-bad-config]}
     :db         (assoc db :tournaments [])
     }
    ))

(rf/reg-event-db
  :fe-process-config
  (fn
    [db [_ config]]
    (println "Event :fe-process-config " config)
    (assoc db :config config)))

(rf/reg-event-db
  :fe-handle-bad-config
  (fn
    [db [_ config]]
    (println "Event :fe-handle-bad-config " config)
    ;; TODO what to do when config fetching fails? use defaults?
    db))

(rf/reg-event-db
  ;; TODO is this actually needed?
  :fe-store-tournament
  (fn
    [db [_ tournament]]
    (println "Event :fe-store-tournament " tournament)
    (update-in db [:tournaments] #(conj % (select-keys tournament [:id :name :creator])))))

(rf/reg-event-db
  :fe-set-active-tournament
  ;; TODO is this needed?
  (fn
    [db [_ tournament-id]]
    (println "Event :set-active-tournament " tournament-id)
    (assoc-in db [:active-tournament] tournament-id)))

(rf/reg-event-db
  :fe-new-player-to-tournament
  (fn
    [db [_ tournament-id player all-players]]
    (println "Event :fe-new-player-to-tournament " tournament-id player all-players)
    (doto
      (specter/transform [:tournaments specter/ALL (specter/if-path [:id (is-key-fn? tournament-id)] :players)] #(vec all-players) db)
      ((fn [x] (println "TODO DOTO REMOVE Tournament" tournament-id "has following players:")
         (->> x
              :tournaments
              (filter #(= (:id %) tournament-id))
              first
              :players)))
      )))

(rf/reg-event-fx
  :fe-tournament-started
  (fn
    [{db :db} [_ tournament-id]]
    (println "Event :fe-tournament-started, tour-id: " tournament-id ", sending :c-ready-for-game")
    {:fe-websocket-send {:type          :fb-ready-for-game
                         :tournament-id tournament-id
                         :user          (:user db)}
     :db                db
     }))

(rf/reg-event-fx
  :fe-create-tournament
  (fn
    [{db :db} [_ name]]
    {:fe-websocket-send {:type :fb-create-tournament
                         :name name
                         :user (:user db)}
     :db                db
     }))

;; Re-join starts

(rf/reg-event-fx
  :fe-rejoin-tournament
  [(rf/inject-cofx ::inject/sub
                   (fn [[_ tournament-id]]
                     [:ongoing-tournaments-game-id tournament-id]))]
  (fn [{db                          :db
        ongoing-tournaments-game-id :ongoing-tournaments-game-id} [_ tournament-id]]
    (println "TODO HERE REJOIN" tournament-id ongoing-tournaments-game-id)
    (if ongoing-tournaments-game-id
      {
       ;; First fetch the ongoing game if any from the BE
       ;; If game found, show it (somehow)
       ;; If no ongoing game, show tournament stats and ready for next game
       ;; Should this inform BE that we are back in the game?
       :fe-websocket-send {:type    :fb-fetch-game
                           :game-id ongoing-tournaments-game-id
                           :user    (:user db)}
       :db                db}
      {:dispatch [:fe-game-not-found tournament-id]
       :db       db})))

(rf/reg-event-fx
  :fe-game-not-found
  (fn
    [{db :db} [_ tournament-id]]
    ;; TODO implement
    {:db db
     }))

(rf/reg-event-fx
  :fe-game-found
  (fn
    [{db :db} [_ {:keys [tournament] :as game}]]
    (println "event-fx :fe-game-found" tournament)

    {
     :db (assoc-in db [:active-tournament] tournament)}))

;; Re-join ends

(rf/reg-event-fx
  :fe-start-tournament
  (fn
    [{db :db} [_ tournament-id]]
    {:fe-websocket-send {:type          :fb-start-tournament
                         :tournament-id tournament-id
                         :user          (:user db)}
     }))

(rf/reg-event-fx
  :fe-join-tournament
  (fn
    [{db :db} [_ tournament-id]]
    {:fe-websocket-send {:type          :fb-join-tournament
                         :tournament-id tournament-id
                         :user          (:user db)}
     :db                (assoc-in db [:active-tournament] tournament-id)}))

(rf/reg-event-fx
  :fe-ready-for-game
  (fn
    [{db :db} [_ tournament-id]]
    {:fe-websocket-send {:type          :fb-ready-for-game
                         :tournament-id tournament-id
                         :user          (:user db)}
     :db                db
     }))

(rf/reg-event-fx
  :fe-join-game
  (fn
    [{db :db} [_ game-id]]
    ;; TODO is this needed, based on messages... and no handler for fb-join-game in the BE.
    {:fe-websocket-send {:type    :fb-join-game
                         :game-id game-id
                         :user    (:user db)}
     :dispatch          [:fe-hide-message {:messageid game-id}]
     :db                db
     }))

(rf/reg-event-fx
  :fe-start-game
  (fn
    [{db :db} [_ game-id]]
    {:fe-websocket-send {:type    :fb-start-game
                         :game-id game-id
                         :user    (:user db)}
     :db                db
     }))

(defn- make-moves-map
  "seq of card+:u/:m/:l pairs to map { :u [...] etc}"
  [m]
  (apply merge
         (map #(hash-map (first %) (hash-map :cards (map first (second %)))) (group-by second m)))
  ;;(reduce (partial merge-with into) (map #(hash-map (second %) [(first %)]) m))
  )


(rf/reg-event-fx
  :fe-cards-placed
  [(rf/inject-cofx ::inject/sub [:game-id])
   (rf/inject-cofx ::inject/sub [:placed-cards])]
  (fn
    [{db           :db
      game-id      :game-id
      placed-cards :placed-cards} _]

    {:fe-websocket-send {:user          (:user db)
                         :game-id       game-id
                         :tournament-id (:active-tournament db)
                         :type          :fb-cards-placed
                         :cards         (make-moves-map placed-cards)
                         }
     :db                db
     }))
(rf/reg-event-db
  :fe-store-last-result
  (fn
    [db [_ lastres]]
    (println "Event :store-last-result " lastres)
    (assoc db :last-result lastres)))

(rf/reg-event-fx
  :fe-hide-message
  (fn
    [{db :db} [_ {:keys [messageid]}]]
    (println "event :hide-message " messageid)
    ;; TODO add this to hidden list OR remove this message completely.
    {:db db}
    ))

(rf/reg-event-db
  :fe-start-game
  (fn
    [db [_ {:keys [players]}]]
    (println "Event :fe-start-game: assigning players " players)
    (-> db
        (assoc :players players
               :game-started true)
        (dissoc :game-ended
                :winners
                :place-cards))
    ))

(rf/reg-event-db
  :fe-end-game
  (fn
    [db [_ {:keys [winners moves]}]]
    (println "Event :game-ended;" winners)
    (-> db
        (assoc :winners winners
               :game-ended true
               :place-cards {:moves moves})
        (dissoc :game-started
                :placed-cards))))

(rf/reg-event-fx
  :fe-process-logout
  (fn
    [{db :db} [_ newpage]]
    (println ":send-logout " newpage)
    (ws/close-websocket!)                                   ;; is this in correct phase?
    {:http-xhrio {:method          :post
                  :uri             "/logout"
                  :timeout         2000
                  :body            (json-tools/clj->json {:username (:user db)})
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [:fe-logout newpage]
                  :on-failure      [:fe-bad-logout]}
     :db         db
     }))

(rf/reg-event-db
  :fe-logout
  (fn [db [_ new-page]]
    (println ":logout called" new-page)
    (-> (only-config db)
        (assoc :page new-page))))

(rf/reg-event-db
  :fe-bad-logout
  (fn
    [db [_ _]]
    (println "Event :bad-logout")
    (only-config db)))

(rf/reg-event-fx
  :fe-send-credentials
  (fn [{db :db} _]
    {:fe-websocket-send {:type  :fb-credentials
                         :user  (:user db)
                         :token (-> db :authentication :token)}
     :db                db
     }))

(rf/reg-event-fx
  :fe-set-user
  (fn
    [{db :db} [_ {:keys [username page]}]]
    (println ":fe-set-user fx for user: " (keyword username))
    (comment :body (doto (js/FormData.)
                     (.append "username" (keyword username)))
             ;;:content-type application/x-www-form-urlencoded
             )
    {:http-xhrio {:method          :post
                  :uri             "/login"
                  :body            (json-tools/clj->json {:username   (keyword username)
                                    :auth-token (-> db
                                                    :authentication
                                                    :token)})
                  :timeout         2000
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [:fe-process-login]
                  :on-failure      [:fe-bad-login]}
     :db         (assoc db :user (keyword username)
                           :page page)
     }))

(rf/reg-event-db
  :fe-login-tokens
  (fn [db [_ {:keys [token]}]]
    ;; TODO this should contain refresh token too
    (update-in db [:authentication :token] (fn [x] token))))

(rf/reg-event-db
  :fe-process-login
  (fn [db [_ loginx]]
    (println ":fe-process-login:" loginx)
    ;; TODO should this use subs to get the protocol config?
    (ws/start-ws (-> db
                     :config
                     :websocket-proto))
    db))

(rf/reg-event-db
  :fe-bad-login
  (fn [db [_ bad-login]]
    (println "process bad-login:" bad-login)
    (assoc db :bad-login bad-login)))

(rf/reg-event-db
  :fe-place-card
  (fn [db [_ card hand]]
    #_(println ":place-card called with " card " and " hand)
    (update-in db [:placed-cards card] (fn [_] hand))))

(rf/reg-event-db
  :fe-place-cards
  (fn [db [_ msg]]
    (println "Event :fe-place-cards:" msg)
    (assoc db :place-cards msg                              ;; hm should this be :moves?
              :placed-cards {})))

(rf/reg-event-db
  :fe-navigate
  (fn [db [_ page]]
    (println ":navigate hit:" page)
    (assoc db :page page)))

(rf/reg-event-fx
  :fe-navigate-to
  (fn [{db :db} [_ new-page-key]]
    (println "TODO :navigated" new-page-key)
    {:navigate! new-page-key
     :db        db}
    ))

(rf/reg-event-db
  :common/set-error
  ;; TODO not used
  (fn [db [_ error]]
    (assoc db :common/error error)))

(rf/reg-event-db
  :fe-add-message
  ;; TODO not used
  (fn [db [_ message]]
    (println ":add-message called with:" message)
    (update-in db [:messages] #(conj % message))))

(rf/reg-event-db
  :fe-add-game-messages
  (fn [db [_ message]]
    (println ":add-game-messages called with:" message)
    (update-in db [:game-messages] #(conj % message))))

(rf/reg-event-db
  :fe-on-going-tournaments
  (fn [db [_ ongoing-tournament]]
    (println ":on-going-tournaments type" (type ongoing-tournament) " and value:" ongoing-tournament)
    (assoc db :on-going-tournaments ongoing-tournament)))

(rf/reg-event-db
  :fe-open-tournaments
  (fn [db [_ tournament-ids]]
    (println "TODO implement :open-tournaments" tournament-ids)
    db))
