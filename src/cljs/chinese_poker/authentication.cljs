(ns chinese-poker.authentication
  (:require ["keycloak-js" :as Keycloak]
            [re-frame.core :as rf]
            ))

(defn init-keycloak []
  (let [keycloak ^Keycloak (Keycloak. (clj->js
                                        ;; TODO get these from the config
                                        {:url      "http://localhost:8080/"
                                         :realm    "chinese"
                                         :clientId "frontend"}))]
    (-> ^js/Promise (.init keycloak #js {:onLoad "login-required"})
        (.then (fn []
                 (if (.-authenticated keycloak)

                   (let [username (-> (.-idTokenParsed keycloak)
                                      js->clj
                                      (get
                                        "preferred_username")
                                      )]
                     ;;(println "Authenticated, keycloak object keys" (js-keys keycloak))

                     (rf/dispatch-sync [:fe-login-tokens {:token (.-token keycloak)}])
                     ;;(println "set user:" username)
                     (rf/dispatch-sync [:fe-set-user {:username username
                                                      :page     :tournament
                                                      }])
                     )
                   (println "Not authenticated"))))
        (.catch (fn [error]
                  (println "Failed to initialize Keycloak:" error))))
    keycloak))
