(ns chinese-poker.websockets
  (:require [re-frame.core :as rf]
            [chinese-poker.serialization-helper :as json-tools]))

(defonce ws-chan (atom nil))
(defn- receive-msg!
  [update-fn channel]
  (fn [msg]
    (println "Receiving via channel:" channel "msg:" (.-data msg))
    (update-fn
      (->> msg .-data json-tools/json->clj))))

(defn send-msg!
  [msg]
  (if @ws-chan
    (.send @ws-chan (json-tools/clj->json msg))
    (throw (js/Error. "Websocket is not available!"))))

(defn close-websocket!
  "Close the websocket"
  []
  (when @ws-chan
    (println "Closing web socket")
    (.close @ws-chan)))

(defn make-websocket! [url receive-handler]
  (println "attempting to connect websocket: " url)
  (if-let [chan (js/WebSocket. url)]
    (do
      (set! (.-onmessage chan) (receive-msg! receive-handler chan))
      (println "Using chan:'" chan "'")
      (close-websocket!)
      (reset! ws-chan chan)
      (println "Websocket connection established with: '" url "'"))
    (throw (js/Error. "Websocket connection failed!"))))



(defmulti handle-msg!
          (fn [msg]
            ;;{:pre [(protocol/valid-msg? msg)]} ;; SPEC for incoming messages
            (do (println "got msg for dispatch:" msg)
                (keyword (:type msg)))))
(defn start-ws
  "Start ws communication"
  [websocket-proto]
  (println "Starting ws connection with:" websocket-proto)
  (make-websocket! (str websocket-proto "://" (.-host js/location) "/ws") handle-msg!))

(defmethod handle-msg! :bf-credential-req
  [msg]
  (println "handle-msg! :bf-credential-req " msg)
  (rf/dispatch [:fe-send-credentials]))

(defmethod handle-msg! :bf-login-info
  [msg]
  (println "handle-msg! :login-info" msg)
  (when-let [open-tours (:open-tournaments msg)]
    (when-not (empty? open-tours)
      (rf/dispatch [:fe-open-tournaments open-tours])))
  (when-let [ongoing (:on-going-tournaments msg)]
    (when-not (empty? ongoing)
      (rf/dispatch [:fe-on-going-tournaments ongoing]))))

(defmethod handle-msg! :bf-tournament-created
  [msg]
  (println "handle-msg! :tournament-created " msg)
  (rf/dispatch [:fe-store-tournament msg])
  ;; TODO mark tournament ready https://github.com/metosin/reitit/blob/master/examples/frontend-re-frame/src/cljs/frontend_re_frame/core.cljs
  )

(defmethod handle-msg! :bf-tournament-started
  [msg]
  (println "handle-msg! :tournament-started " msg)
  (rf/dispatch [:fe-tournament-started (:tournament-id msg)]))

(defmethod handle-msg! :bf-player-joined
  [{:keys [tournament-id player players] :as msg}]
  (println "handle-msg! :player-joined " msg)
  ;; "Received :player-joined message TODO mark game ready"
  (rf/dispatch [:fe-new-player-to-tournament tournament-id player players]))

(defmethod handle-msg! :bf-place-cards
  [msg]
  (println "handle-msg! :place-cards " msg)
  (rf/dispatch [:fe-place-cards
                {:msg     "Got cards"
                 :cards   (:cards msg)
                 :game-id (:game-id msg)
                 :moves   (:moves msg)
                 :next    (:next msg)}]))

(defmethod handle-msg! :bf-start-game
  [msg]
  (println "handle-msg! :start-game" msg)
  (rf/dispatch-sync [:fe-start-game
                     {
                      :players (:players msg)
                      }])
  (rf/dispatch-sync [:fe-place-cards
                     {:msg     "Got cards"
                      :cards   (:cards msg)
                      :game-id (:game-id msg)
                      :moves   (:moves msg)
                      :next    (:next msg)}])
  (rf/dispatch [:fe-navigate :game])
  )

(defmethod handle-msg! :bf-game-ended
  [msg]
  (println "handle-msg! :game-ended" msg)
  (println "Received :game-ended message winners:" (:winners msg))
  (rf/dispatch-sync [:fe-end-game
                     {:winners (:winners msg)
                      :moves   (:moves msg)}])
  (rf/dispatch [:fe-add-game-messages
                {
                 :msg     "Game ended!"
                 :type    :fe-game-ended
                 :id      (:id msg)
                 :winners (:winners msg)}]))

(defmethod handle-msg! :bf-game-found
  [msg]
  (println "handle-msg! :bf-game-found " msg)
  ;; TODO this is a hackish way to do this. See backends websockets :fb-fetch-game handling.
  (rf/dispatch-sync [:fe-game-found msg])
  (handle-msg! (assoc msg :type :bf-start-game))
  )

(defmethod handle-msg! :bf-game-not-found
  [msg]
  ;; TODO is this the correct place to do game re-initialization logic?
  (println "handle-msg! :bf-game-not-found " msg))

(defmethod handle-msg! :bf-tournament-update
  [msg]
  (println "handle-msg! :bf-tournament-update" msg)
  (rf/dispatch [:fe-store-last-result {:msg      "Tournament result update"
                                    :totalpoints (:totalpoints msg)
                                    :previous    (:previous msg)
                                    }]))

