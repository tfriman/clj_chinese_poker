(ns chinese-poker.serialization-helper)

(defn- keywordize-if-not-already
  "Self-explanatory..."
  [in]
  (if (keyword? in)
    in
    (keyword (str in))))

(defn- fix-json
  "Recursively keywordizes the values of a map based on a predicate function."
  [m exclude-set]
  (let [path []
        predicate (fn [x] (nil? (exclude-set x)))]
    (letfn [(keywordize-helper
              [value path predicate]
              (cond
                (map? value) (reduce-kv (fn [acc k v]
                                          (assoc acc k (keywordize-helper v (conj path k) predicate)))
                                        {} value)
                (vector? value) (mapv #(keywordize-helper % path predicate) value)
                (number? value) value                       ;; leaves all numbers as is.
                (predicate path) (keywordize-if-not-already value)
                :else value))]
      (keywordize-helper m path predicate))))

(def type-map
  "Contains sets for those keys in format [:a :b :c], [:a :d] which should not be keywordized"
  {:bar #{[:profile :name] [:email]}
   :bf-player-joined #{[:token]}
   })

(defn msg->map
  "maps incoming json to edn map with keyword keys and also uses mappings to map values for matching type's keys to
  keywords. Expects type and user to present in the json message."
  ([msg]
   (msg->map type-map msg))
  ([mappings msg]
   {:post [(keyword? (:type %))]}
   (let [e-map msg
         e-type (keywordize-if-not-already (:type e-map))
         _ (def e-map e-map)
         _ (def e-type e-type)]
     (assert (and (not (nil? e-type)) (keyword? e-type)) (str "Incoming message had no type or it wasn't a keyword:" msg))
     (fix-json e-map (or (e-type mappings) #{}))
     )))

(defn clj->json
  [ds]
  (.stringify js/JSON (clj->js ds)))

(defn json->clj
  [ds]
  (let [result (js->clj (.parse js/JSON ds) :keywordize-keys true)
        fixed (msg->map result)]
    (println "json->clj result:" result)
    (println "json->clj fixed:" fixed)
    fixed)
  )