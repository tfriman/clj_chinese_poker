(ns chinese-poker.views
  (:require [clojure.string :as string]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [chinese-poker.authentication :as authentication]))

(declare winner-view)                                       ;; todo is this needed
(defn create-game []
  (if-let [tournament-id @(rf/subscribe [:active-tournament])]
    (r/with-let [user @(rf/subscribe [:user])
                 visible? (r/atom true)]
                (when @visible?
                  [:button
                   {:on-click (fn []
                                (rf/dispatch [:fe-ready-for-game tournament-id])
                                (reset! visible? false))
                    :class    "button"
                    :display  (if @visible? "inline" "none")
                    }
                   "Ready for a new game"]))
    "Start tournament first"))

(defn join-game [game-id creator]
  ;; TODO is this needed, messages do not work currently?
  (r/with-let [visible? (r/atom true)
               user @(rf/subscribe [:user])]
              (when @visible?
                [:button
                 ^{:key (str game-id)}
                 {:on-click (fn []
                              (rf/dispatch [:fe-join-game game-id])
                              (reset! visible? false))
                  :class    "button"
                  :display  (if @visible? "inline" "none")
                  }
                 (str "Join game by " creator " and id:" game-id)]
                )))

(defn start-game [game-id]
  (fn []
    [:button
     {:on-click #(rf/dispatch [:fe-start-game game-id])
      :class    "button"}
     (str "Start tournament:" game-id)]))

(defn- card-component
  "Presents a card"
  [{:keys [display]}]

  (let [displaystr (name display)]
    [:img {:src    (str "/img/cards/" displaystr ".jpg")
           :width  40
           :height 40
           :alt    displaystr
           :title  displaystr}]))

(defn- set-card-hand!
  [card hand]
  #_(println "set-card-hand! " card " " hand)
  (rf/dispatch [:fe-place-card card hand]))

(defn- hand-selector-button
  [card street selected-atom active?]
  [:button
   {:on-click (fn []
                (set-card-hand! card street)
                (reset! selected-atom street))
    :class    (str "button" (when active? " is-static"))}
   (if (= street @selected-atom)
     (string/upper-case (name street))
     (name street))])

(defn- selector-helper
  [card street-key selected]
  (hand-selector-button card street-key selected @(rf/subscribe [:is-street-filled? street-key])))

(defn- hand-selector-for-card
  "u/m/l selector for card"
  [card]
  (r/with-let [selected (r/atom nil)]
              [:span
               (selector-helper card :u selected)
               (selector-helper card :m selected)
               (selector-helper card :l selected)
               ]))

(defn- card-selectable
  [card]
  [:div.column
   [:div.box
    [card-component card]
    [hand-selector-for-card card]
    ]])

(defn- send-card-placements []
  (fn []
    (let [can-send? (and @(rf/subscribe [:ready-to-send?])
                         @(rf/subscribe [:game-started]))]
      [:div.container
       [:div.column
        [:button
         {:on-click #(rf/dispatch [:fe-cards-placed])
          :class    (str "button"
                         (when-not
                           can-send?
                           " is-static"))}
         "Send cards"]]])))

(defn card-list []
  (let [cards (map-indexed vector @(rf/subscribe [:cards-to-place-for-player]))]
    [:div.columns.is-mobile
     (for [[i card] cards]
       ^{:key i}
       [card-selectable card])]))

(defn- show-given-street [street-name moves winner-flag]
  [:div.columns.is-mobile
   [:div.column.is-1
    {:class (when winner-flag "winner-row")}
    (str street-name (when winner-flag "WINNER!"))]
   (for [[i card] (map-indexed vector moves)]
     ^{:key (str "street" i (:display card))}
     [:div.column.is-1
      [:article.media
       [:div.media-content
        [:div.content
         [card-component card]
         ]]]]
     )])

(def street-keys {:u "Upper"
                 :m  "Middle"
                 :l  "Low"})

(defn- street-helper
  [moves street-key]
  (show-given-street (street-key street-keys) (get-in moves [street-key :cards]) (get-in moves [street-key :winner])))

(defn show-placed-cards [name moves]
  [:div.container
   [:div.columns
    [:div.column.is-one-fifth
     [:h3 (str "Player:" name)]]]
   (street-helper moves :u)
   (street-helper moves :m)
   (street-helper moves :l)])

(defn- show-all-placed-cards []
  (let [moves @(rf/subscribe [:moves])]
    [:div.container
     (let [player-id @(rf/subscribe [:this-player])]
       [:div.columns.is-mobile
        ^{:key (str "card" (first player-id))}
        (show-placed-cards (str "YOU " (second player-id)) (get-in moves [(first player-id)]))])
     (let [other-players (map-indexed vector @(rf/subscribe [:other-players]))]
       (for [[i player-id] other-players]
         ^{:key (str i (first player-id))}
         [:div.columns.is-mobile
          (show-placed-cards (second player-id) (get-in moves [(first player-id)]))]))
     ]))

(defn game-messages
  []
  [:ul
   (let [messages (map-indexed vector @(rf/subscribe [:new-game-messages]))]
     (for [[i message] messages]
       ^{:key (str i (:id message))}
       [:li
        [join-game (:id message) (:creator message)]]))
   (let [own-games (map-indexed vector @(rf/subscribe [:own-games]))]
     (for [[i message] own-games]
       ^{:key (str "sub-own" i (:id message))}
       [:li
        [start-game (:id message)]]
       ))])

(defn game-page
  []
  [:div.container
   [:div.columns
    [:div.column.is-one-fifth
     [:h2 (str "Welcome to the Chinese Poker, " @(rf/subscribe [:user]))]]
    [:div.column.is-one-fifth
     (when-not @(rf/subscribe [:game-started])
       [create-game])
     ]
    [:div.column.auto
     [game-messages]]
    ]
   [card-list]
   [:div.columns
    ;; think this model... adds div.container
    (when @(rf/subscribe [:game-started])
      [send-card-placements]
      )
    ]
   ;; adds div.container
   (when @(rf/subscribe [:game-started])
     [show-all-placed-cards]
     )
   (when @(rf/subscribe [:game-ended])
     [winner-view]
     )
   (when @(rf/subscribe [:game-ended])
     [show-all-placed-cards])
   ])

(defn nav-link [uri title page]
  [:a.navbar-item
   {:href  uri
    :class (when (= page @(rf/subscribe [:page])) :is-active)}
   title])

(defn navbar []
  (r/with-let [expanded? (r/atom false)
               logged-in? (rf/subscribe [:logged-in?])]
              [:nav.navbar.is-info>div.container
               [:div.navbar-brand
                [:a.navbar-item {:href "/" :style {:font-weight :bold}} "Chinese Poker"]
                [:span.navbar-burger.burger
                 {:data-target :nav-menu
                  :on-click    #(swap! expanded? not)
                  :class       (when @expanded? :is-active)}
                 [:span] [:span] [:span]]]
               [:div#nav-menu.navbar-menu
                {:class (when @expanded? :is-active)}
                [:div.navbar-start
                 [nav-link "#/tournament" "Tournaments" :tournament]
                 [nav-link "#/game" "Game" :game]
                 (comment [nav-link "#/messages" "Messages" :messages])
                 (if @logged-in?
                   [nav-link "#/logout" "Logout" :logout]
                   [nav-link "#/" "Login" :login]
                   )
                 ]]]))

(defn- winner-selector
  []
  (let [winners @(rf/subscribe [:winners])
        players @(rf/subscribe [:players])]
    ;; TODO how to show winning hands?
    ;; option a: create a new view
    ;; option b: is dynamic selector or subs possible? key generation: player(p1 or name?)-hand(:u)
    ;; option c: update hands and flag winning hands. this is the way probably to go.
    [:p
     (str "Winners found:"
          " Upper:" (clojure.string/join "," (map #(% players) (:u winners)))
          " Middle:" (clojure.string/join "," (map #(% players) (:m winners)))
          " Lower:" (clojure.string/join "," (map #(% players) (:l winners))))
     ]))

(defn winner-view
  []
  [:div
   (if @(rf/subscribe [:game-ended])
     [winner-selector]
     [:p "No Winners in this hand!"])
   ])

(comment
  ;; TODO remove me
  (defn login-page-form
    []
    (let [form (r/atom {})]
      (fn []
        [:form
         {:on-submit (fn [e]
                       (.preventDefault e)
                       (when (:username @form)
                         ;; TODO remove fe-set-user
                         (rf/dispatch [:fe-set-user {:username (:username @form)
                                                     :page     :tournament
                                                     }])
                         ))}
         [:label "Username"]
         [:input
          {:default-value ""
           :on-change     #(swap! form assoc :username (-> % .-target .-value))}]
         [:button
          {:type "submit"}
          "Login"]]))))

(defn tournament-create-form []
  (let [form (r/atom {})]
    (fn []                                                  ;; TODO is this needed?
      [:form
       {:on-submit (fn [e]
                     (.preventDefault e)
                     (when (:name @form)
                       (rf/dispatch [:fe-create-tournament (:name @form)])
                       ))}
       [:label "Name"]
       [:input
        {:default-value ""
         :on-change     #(swap! form assoc :name (-> % .-target .-value))}]
       [:button
        {:type "submit"}
        "Create tournament"]
       ])))

(defn tournament-results []
  (if-let [results @(rf/subscribe [:last-result])]
    [:div.container
     [:div.columns
      [:div.column
       [:h3 "Tournament total points"]]]
     [:div.columns
      (for [[player points] (:totalpoints results)]
        ^{:key (str "t-result-for" player)}
        [:div.column.is-one-fourth
         [:h3 (str "Player " player)]
         points
         ])]
     [:div.columns
      [:div.column
       [:h3 "Previous round results"]]]
     [:div.columns
      (for [[player points] (-> results :previous :points)]
        ^{:key (str "prev-result-for" player)}
        [:div.column.is-one-fourth
         [:h3 (str "Player " player)]
         points
         ])
      ]]))

(defn register-tournament [user tournament-id name creator]
  (let [players @(rf/subscribe [:tournament-players tournament-id])]
    [:div.columns
     ^{:key (str "col" tournament-id)}
     [:div.column.is-one-fourth
      (str "Tournament " name " by " creator " has " (count players) " registered players.")
      (if (some #{user} players)
        (when (= user creator)
          [:button
           {:on-click #(rf/dispatch [:fe-start-tournament tournament-id])
            :class    "button"}
           "Start"])
        [:button
         {:on-click #(rf/dispatch [:fe-join-tournament tournament-id]
                                  #_(rf/dispatch [:fe-set-active-tournament tournament-id])
                                  )
          :class    "button"}
         "Join"]
        )]]))

(defn rejoin-tournament [tournament-id name creator]
  (let [players @(rf/subscribe [:ongoing-tournament-players tournament-id])]
    [:div.columns
     ^{:key (str "col" tournament-id)}
     [:div.column.is-one-fourth
      (str "Tournament " name " by " creator " has players:" (clojure.string/join ", " players))
      [:button
       {:on-click #(rf/dispatch [:fe-rejoin-tournament tournament-id])
        :class    "button"}
       "Rejoin"]]]))

(defn tournament-list []
  (when-let [tournaments @(rf/subscribe [:tournaments])]
    (when-let [user @(rf/subscribe [:user])]
      [:div.container
       [:div.container
        [:p "Tournaments you can register to:"]]
       [:div.container
        (for [{:keys [id name creator]} tournaments]
          ^{:key (str "cols" id)}
          [register-tournament user id name creator]
          )]])))

(defn ongoing-tournament-list []
  (when-let [tournaments @(rf/subscribe [:on-going-tournaments])]
    [:div.container
     [:div.container
      [:p "Tournaments you are already playing:"]]
     [:div.container
      (for [{:keys [id name creator]} tournaments]
        ^{:key (str "cols" id)}
        [rejoin-tournament id name creator]
        )]]))


(defn secure-component []
  (let [keycloak (authentication/init-keycloak)]
    (println "keycloak secure component")
    (def keycloak keycloak)
    (if (.-authenticated keycloak)
      ;; Return the secured component
      [:div "This is a secured part of the app."]
      ;; User is not authenticated, so redirect or display a message
      [:div "Please log in to access this part of the app."])))

(defn tournament-page
  "Create and manage tournaments"
  []
  [:div.container>div.content
   [tournament-create-form]
   [ongoing-tournament-list]
   [tournament-list]
   [tournament-results]])

(defn login-page []
  [:div.container>div.content
   [:a
    {:href "/oauth2/github"
     }
    "Login via github"]
   ])

(defn logout-page []
  [:div.container>div.content
   [:button
    {:on-click #(rf/dispatch [:fe-process-logout :login])
     :class    "button"}
    "Logout"]])

(def pages
  {:tournament #'tournament-page
   :game       #'game-page
   :login      #'login-page
   :logout     #'logout-page})

(defn poker-app
  []
  [:<>
   [:section#poker-app
    [navbar]
    [(pages @(rf/subscribe [:page]))]
    ]
   [:footer#info
    [secure-component]
    [:div.container>div.content "Chinese Poker Application."]]
   ])
