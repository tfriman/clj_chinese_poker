(ns chinese-poker.core
  (:require
    [chinese-poker.ajax :as ajax]
    [chinese-poker.events]
    [chinese-poker.router :as router]
    [chinese-poker.subs]
    [chinese-poker.views :as views]
    [day8.re-frame.http-fx]
    [re-frame.core :as rf]
    [re-frame.db]
    [reagent.dom :as rdom]
    [reitit.frontend.easy :as rfe]
    ))

;; -------------------------
;; Initialize app
(defn mount-components []

  (rdom/render [views/poker-app] (.getElementById js/document "app")))

(defn ^:after-load re-render []
  (mount-components))

;; see https://betweentwoparens.com/start-a-clojurescript-app-from-scratch#add-reagent (defonce start-up (do (mount) true))

(defn on-navigate [new-match]
  (when new-match
    (prn "on-navigate called: " new-match)
    (rf/dispatch [:fe-navigate (-> new-match :data :name)])))

(defn init []
  (rf/clear-subscription-cache!)
  (rf/dispatch-sync [:fe-initialize-db])
  (rf/dispatch-sync [:fe-fetch-config])
  (rfe/start! router/router
              on-navigate
              {:use-fragment true})
  (rf/dispatch-sync [:fe-navigate :login])
  (ajax/load-interceptors!)
  (mount-components))
