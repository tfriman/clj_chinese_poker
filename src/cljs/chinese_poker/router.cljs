(ns chinese-poker.router
  (:require [reitit.coercion.spec :as rss]
            [reitit.core :as reitit]))

(def router
  (reitit/router
    [
     ["/" :login]
     ["/tournament" :tournament]
     ["/game" :game]
     ["/logout" :logout]
     ]
    {:data {:coercion rss/coercion}}))
