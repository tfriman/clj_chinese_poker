(ns chinese-poker.tools.autoplayerbot
  (:require
    [chinese-poker.helpers :as helpers]
    [clojure.core.async :as async]
    [gniazdo.core :as ws]
    ))

;; Plays against websocket.
;; Simple heuristic, place cards in order.
;; Params port player tournament

(def port 3000)

(defn- socket
  "Create socket connection. Store responses to a (atom)"
  [channel]
  (ws/connect (str "ws://localhost:" port "/ws")
              :on-receive (fn [m]
                            (let [msg (helpers/msg->map m)]
                              (async/go (async/>! channel msg))))
              :on-connect #(prn 'connected %)))

(defn testsend [socket]
  (fn [msg]
    (println "testsend:" msg)
    (ws/send-msg socket (helpers/msg-as-json msg))
    (Thread/sleep 50)))

(def tournamentupdates (atom ()))

(defn statemachine [player sender gamecountmax manage-tournament?]
  (let [playermapping (atom nil)
        tournament-id (atom nil)
        playercount (atom 0)                                ;; todo change these to one atom {}
        gamecount (atom gamecountmax)
        ]
    (fn [msg]
      (println "Statemachine got playermapping" @playermapping " msg " msg)
      (let [type (:type msg)]
        ;; todo refactor me
        (condp = type
          :bf-login-info (when-not manage-tournament?
                        (let [opentours (:open-tournaments msg)
                              _ (println "login info got " (type opentours))
                              tournament-idfirst (-> opentours first)]
                          (println "joining tournament-id" tournament-idfirst)
                          (reset! tournament-id tournament-idfirst)
                          (sender {:user          player
                                   :type          :fb-join-tournament
                                   :tournament-id tournament-idfirst})))
          :bf-tournament-created (when (and manage-tournament? (nil? @tournament-id) (= player (:creator msg)))
                                (println "tournament-created, joining it")
                                (let [tournament-idmsg (:id msg)]
                                  (reset! tournament-id tournament-idmsg)
                                  (sender {:user          player
                                           :type          :fb-join-tournament
                                           :tournament-id tournament-idmsg})))
          :bf-player-joined (when manage-tournament?
                              (println "managing tournament:" @playercount "and msg" msg)
                              (when (= 2 (swap! playercount inc))
                                (println "sending start tournament")
                                (sender {:user       player
                                      :tournament-id @tournament-id
                                      :type          :fb-start-tournament})))
          :bf-tournament-update (do
                                  (swap! tournamentupdates conj msg)
                                  (if (= 0 (swap! gamecount dec))
                                    (do
                                   (println "gamecount done, exiting" player)
                                   ::done)
                                    (do
                                      ;; duplicate code here.
                                      ;;(println "sm tournament updated, sending ready")
                                      (sender {:type :fb-ready-for-game, :tournament-id @tournament-id, :user player}))))
          :bf-tournament-started (do
                                   (println "sm tournament started, sending ready")
                                   (sender {:type :fb-ready-for-game, :tournament-id @tournament-id, :user player}))
          :bf-start-game (do
                        (println "received game-started" (:players msg))
                        (reset! playermapping (player (clojure.set/map-invert (:players msg))))
                        (let [game-id (:game-id msg)
                              cards (:cards msg)
                              playertarget (:next msg)
                              ]
                          (when (= @playermapping playertarget)
                            (let [cardcount (-> msg
                                                :moves
                                                playertarget
                                                vals
                                                ((fn [v] (map #(-> % vals first count) v)))
                                                ((fn [x] (apply + x)))
                                                )
                                  hand (if (> cardcount 9) :l (if (> cardcount 4) :m :u))
                                  cardsplacement (into {} (map (fn [c] [c hand]) cards))
                                  ]
                              (sender {:cards         cardsplacement
                                       :user          player
                                       :tournament-id @tournament-id
                                       :game-id       game-id
                                       :type          :fb-cards-placed
                                       })))))
          :bf-place-cards (let [game-id (:game-id msg)
                             cards (:cards msg)
                             playertarget (:next msg)
                             ]
                         (when (= @playermapping playertarget)
                           (let [cardcount (-> msg
                                               :moves
                                               playertarget
                                               vals
                                               ((fn [v] (map #(-> % vals first count) v)))
                                               ((fn [x] (apply + x)))
                                               )
                                 hand (if (> cardcount 9) :l (if (> cardcount 4) :m :u))
                                 cardsplacement (into {} (map (fn [c] [c hand]) cards))

                                 ]
                             ;;   :cards {{:display "8H"} :u}}
                             (sender {:cards         cardsplacement
                                      :user          player
                                      :tournament-id @tournament-id
                                      :game-id       game-id
                                      :type          :fb-cards-placed
                                      }))))
          ::stop (do
                   (println "state was ::stop")
                   ::done)
          nil)))))

(defn listen-loop [player channel smachine]
  (async/go-loop []
    (when-let [msg (async/<! channel)]
      (if (= ::done (smachine msg))                         ;; not best practise to have side effect with if...
        (println "play" player "stopped")
        (do
          ;;(println "go-loop got message" msg)
          (recur))))))

(defn play [player channel gamecount]
  (let [socket (socket channel)
        sender (testsend socket)
        smachine (statemachine player sender gamecount false)]

    (listen-loop player channel smachine)

    (sender {:type :fb-credentials
             :user player})))

(defn create-and-play [player channel gamecount]
  (let [socket (socket channel)
        sender (testsend socket)
        smachine (statemachine player sender gamecount true)]
    (listen-loop player channel smachine)

    (sender {:type :fb-credentials
             :user player})

    (sender {:user player
             :type :fb-create-tournament
             :name (str "autobottournament" (rand-int 100))})))

(def chans (atom []))

(comment
  (defn ch [a]
    (let [c (async/chan)]
      (swap! a conj c)
      c))

  (reset! tournamentupdates (atom ()))

  ;;  @tournamentupdates

  (def gamecount 10)

  (create-and-play :foobot (ch chans) gamecount)

  (play :testplayer1 (ch chans) gamecount)

  ;;(create-and-play :player1 (ch chans) 2)
  ;;(play :player2 (ch chans) 2)
  ;; player1 gets 3 points!
  (for [c @chans]
    (async/put! c {:type ::stop}))
  )
