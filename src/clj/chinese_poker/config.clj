(ns chinese-poker.config
  (:require
    [cprop.core]
    [cprop.source :as source]
    [mount.core :as mount]))

(mount/defstate env
                :start
                (cprop.core/load-config
                  :merge
                  [(mount/args)
                   (source/from-system-props)
                   (source/from-env)]))
