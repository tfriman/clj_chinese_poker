(ns chinese-poker.poker.valuator)

(def replacements {\A 14, \K 13, \Q 12, \J 11, \T 10, \9 9, \8 8, \7 7, \6 6, \5 5, \4 4, \3 3, \2 2})

(defn- rank [[r _]]
  (replacements r))

(defn suit [[_ s]]
  (str s))

(defn- ranks [hand]
  (map rank hand))

(defn- freqs [hand]
  (frequencies (ranks hand)))

(defn- freqs-contains [hand c]
  (seq (filter (fn [[_ b]] (= c b)) (freqs hand))))

(defn pair? [hand]
  (freqs-contains hand 2))

(defn three-of-a-kind? [hand]
  (freqs-contains hand 3))

(defn four-of-a-kind? [hand]
  (freqs-contains hand 4))

(defn flush? [hand]
  (seq (filter (fn [[_ b]] (= 5 b)) (frequencies (map suit hand)))))

(defn full-house? [hand]
  (and (freqs-contains hand 3) (freqs-contains hand 2)))

(defn two-pairs? [hand]
  (and (pair? hand) (= 3 (count (freqs hand)))))

(defn straight? [hand]
  (let [rs (ranks hand)
        sorted (sort rs)
        low (first sorted)
        scaled (map #(- % low) sorted)
        normal (range 0 5)
        ace (sort (conj (range 0 4) 12))]
    (or (= scaled normal) (= scaled ace))))

(defn straight-flush? [hand]
  (and (straight? hand) (flush? hand)))

(defn high-card? [_]
  true)

(defn hand-keyword-converter
  "Converts hands with keywords to names."
  ([fn h1 h2]
   (fn (map name h1) (map name h2)))
  ([fn h1]
   (fn (map name h1)))
  ([h1]
   (map name h1)))

(defn value
  "gives back number telling the hand strength. Input is keyworded stuff, converts to name"
  [hand]
  (def hand hand)
  (let [checkers #{[high-card? 0] [pair? 1]
                   [two-pairs? 2] [three-of-a-kind? 3]
                   [straight? 4] [flush? 5]
                   [full-house? 6] [four-of-a-kind? 7]
                   [straight-flush? 8]}]
    (-> hand
        hand-keyword-converter
        (as-> h
              (filter (fn [[f _]] (f h)) checkers)
              (sort-by second > h))
        first
        second)))

(def multipliers [100000000 1000000 10000 100 1])

(defn high-card-comp
  "compares high-cards hands -1/0/1"
  [h1 h2]
  ;;(prn "high-card-comp, always at this point" h1 h2)
  (let [s1 (sort > (map rank h1))
        s2 (sort > (map rank h2))
        r1 (reduce + (map * s1 multipliers))
        r2 (reduce + (map * s2 multipliers))]
    (if (> r1 r2)
      1
      (if (< r1 r2)
        -1
        0))))

(defn pair-comp
  "Compares pairs, returns -1/0/1
  How about ties!!! TODO"
  [h1 h2]
  ;;(prn "pair-comp" h1 h2)
  (let [s1 (sort-by last > (frequencies (map rank h1)))
        s2 (sort-by last > (frequencies (map rank h2)))
        p1 (ffirst s1)
        p2 (ffirst s2)]
    (if (> p1 p2)
      1
      (if (< p1 p2)
        -1
        (high-card-comp h1 h2)))))

(defn sorted-by-key-and-val
  "Sort map first by key and if matching then by val."
  [mapping]
  (into (sorted-map-by
          (fn [key1 key2]
            (compare [(get mapping key2) key2]
                     [(get mapping key1) key1])))
        mapping))

(defn two-pair-comp
  "compares two pair hands"
  [h1 h2]
  (let [s1 (vec (keys (sorted-by-key-and-val (frequencies (map rank h1)))))
        s2 (vec (keys (sorted-by-key-and-val (frequencies (map rank h2)))))]
    (compare s1 s2)))

(defn set-comp
  "compares set hands"
  [h1 h2]
  (let [s1 (vec (keys (sorted-by-key-and-val (frequencies (map rank h1)))))
        s2 (vec (keys (sorted-by-key-and-val (frequencies (map rank h2)))))]
    (compare s1 s2)))

(defn- a5fixer
  "Fixes a5 straight to have A as 1 instead of 14. Expects vector in. TODO add pre cond"
  [ranks]
  (if (and (some #{14} ranks) (some #{5} ranks)) (-> (set ranks) (disj 14) (conj 1) vec) ranks))

(defn straight-comp
  "compares straight hands"
  [h1 h2]
  (let [s1 (vec (keys (sorted-by-key-and-val (frequencies (a5fixer (mapv rank h1))))))
        s2 (vec (keys (sorted-by-key-and-val (frequencies (a5fixer (mapv rank h2))))))]
    (compare s1 s2)))

(defn flush-comp
  "compares flush hands"
  [h1 h2]
  (let [s1 (vec (keys (sorted-by-key-and-val (frequencies (map rank h1)))))
        s2 (vec (keys (sorted-by-key-and-val (frequencies (map rank h2)))))]
    (compare s1 s2)))

(defn full-house-comp
  "compares full house hands"
  [h1 h2]
  (let [s1 (vec (keys (sorted-by-key-and-val (frequencies (map rank h1)))))
        s2 (vec (keys (sorted-by-key-and-val (frequencies (map rank h2)))))]
    (compare s1 s2)))
(defn four-of-kind-comp
  "compares four of a kinds"
  [h1 h2]
  (let [s1 (vec (keys (sorted-by-key-and-val (frequencies (map rank h1)))))
        s2 (vec (keys (sorted-by-key-and-val (frequencies (map rank h2)))))]
    (compare s1 s2))
  )
(def hand-class-comparators {:0 high-card-comp
                             :1 pair-comp
                             :2 two-pair-comp
                             :3 set-comp
                             :4 straight-comp
                             :5 flush-comp
                             :6 full-house-comp
                             :7 four-of-kind-comp
                             :8 straight-comp})

(defn get-hand-comparator
  "Gets comparator for hand class
  TODO remove default value when done"
  [hclass]
  (partial hand-keyword-converter
           ((keyword (str hclass)) hand-class-comparators high-card-comp)))
