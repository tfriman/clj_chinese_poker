(ns chinese-poker.poker.deck)

;; this contains card deck

(def deck '(
            {:display :AH}
            {:display :KH}
            {:display :QH}
            {:display :JH}
            {:display :TH}
            {:display :9H}
            {:display :8H}
            {:display :7H}
            {:display :6H}
            {:display :5H}
            {:display :4H}
            {:display :3H}
            {:display :2H}

            {:display :AS}
            {:display :KS}
            {:display :QS}
            {:display :JS}
            {:display :TS}
            {:display :9S}
            {:display :8S}
            {:display :7S}
            {:display :6S}
            {:display :5S}
            {:display :4S}
            {:display :3S}
            {:display :2S}

            {:display :AD}
            {:display :KD}
            {:display :QD}
            {:display :JD}
            {:display :TD}
            {:display :9D}
            {:display :8D}
            {:display :7D}
            {:display :6D}
            {:display :5D}
            {:display :4D}
            {:display :3D}
            {:display :2D}

            {:display :AC}
            {:display :KC}
            {:display :QC}
            {:display :JC}
            {:display :TC}
            {:display :9C}
            {:display :8C}
            {:display :7C}
            {:display :6C}
            {:display :5C}
            {:display :4C}
            {:display :3C}
            {:display :2C}
            ))
