(ns chinese-poker.gamecontroller
  (:require [chinese-poker.engine :as engine]
            [chinese-poker.helpers :as helper]
            [chinese-poker.persistence.persistence :as persistence]
            [chinese-poker.poker.valuator :as valuator]
            [clojure.tools.logging :as log]))

(defn open-tournaments
  "Fetch tournaments accepting players."
  []
  (let [ll (filter #(= :created (-> % second :status)) (persistence/tournaments))
        ;; note skips other details. used now for autoplayerbots...
        result (mapv first ll)
        ]
    result))

(defn ongoing-tournaments
  [player]
  (log/info "Searching ongoing tournaments for " player)
  (persistence/ongoing-tournaments-for-player player))

(defn tournament-status
  "Get tournament status, should be keyword"
  ;; todo remove whole function
  [tournament-id]
  (if (keyword? tournament-id)
    (persistence/tournament tournament-id)
    (throw (RuntimeException. (str "tournament-id " tournament-id "was not keyword!")))))

(defn get-player-names-for-tournament
  "Self explanatory"
  [tid]
  (:players (persistence/tournament tid)))

(defn create-game
  "Create game, return game id"
  [{playerid :user tournament-id :tournament-id :as arg}]
  {:pre [(or (:user arg)
             (throw (Exception. (format "Pre-condition failed; player-id is empty."))))
         (or (:tournament-id arg)
             (throw (Exception. (format "Pre-condition failed; tournament-id is empty."))))
         ]}
  (let [id (keyword (helper/uuid))
        tournament (persistence/tournament tournament-id)
        playerk (helper/keywordize-if-not-already playerid)]
    (persistence/store-game! {:creator    playerk
                              :tournament tournament-id
                              :id         id
                              :state      :created
                              :created    (java.util.Date.) ;; TODO change?
                              :players    (:players tournament)
                              })
    id))

(defn start-game
  "Starts game; creates one and stores it."                 ;; TODO this should be private, mark-player-ready should handle this
  [{player-id :user tournament-id :tournament-id}]
  (let [game-id (create-game {:user player-id :tournament-id tournament-id})
        players (get-player-names-for-tournament tournament-id)
        tournament-data (tournament-status tournament-id)
        game (merge (engine/create-game (count players) players (count (:results tournament-data))) {:tournament tournament-id})
        p1c (engine/get-n-cards-for-player game 1 5 0)
        ;; TODO architectural problem, :type is here
        result {:type    :bf-start-game
                :cards   p1c
                :game-id game-id
                :players (:idtoplayermapping game)
                :next    :p1}
        ]
    (persistence/moves-to-game! game-id game)
    result
    ;; TODO removed for testing (reset! gamestore game)
    ))

(defn fetch-game
  "Searches for given game"
  [game-id]
  (persistence/game game-id))

(defn mark-player-ready
  "Mark player-id ready for tournament-id. Replies either :status :waiting or :ready"
  [player-id tournament-id]
  (log/infof "mark-player-ready %s for tour %s" player-id tournament-id)
  (let [tournament (persistence/player-ready! tournament-id player-id)
        player-count (-> tournament :players count)
        ready-count (-> tournament :ready-count count)]
    (if (= player-count ready-count)
      (do
        (persistence/mark-tournament-ready! tournament-id)
        {:status :ready
         ;; TODO why is this needed at all?
         :game   (start-game {:user player-id :tournament-id tournament-id})})
      {:status :waiting})))

(defn join-tournament
  "Join tournament, tournament-id keyword"
  [playerid tournament-id]
  (persistence/player-to-tournament tournament-id playerid))

(defn create-tournament
  "Creates tournament."
  [{user :user name :name}]
  (let [id (keyword (helper/uuid))]
    #_(log/info "Created tournament for user " user " with id " id)
    (persistence/new-tournament {:creator     user
                                 :id          id
                                 :results     []
                                 :name        name
                                 :ready-count #{}
                                 :status      :created
                                 :created    (java.util.Date.) ;; TODO change?
                                 })
    id))

(defn start-tournament
  "Mark tournament started"
  [{:keys [user tournament-id]}]
  (try
    (persistence/update-tournament-status tournament-id :started)
    (catch Throwable t
      (log/errorf "start-tournament failed for user %s and tournament %s" user tournament-id)
      (throw t))))

(defn placed-cards-handler
  "Will store placed cards"
  [{:keys [user cards game-id]}]
  ;; TODO how to see who is the player 1?
  ;; is this race condition free way to do this? todo security
  ;; todo add checking game ids match. security!
  (let [game (-> (persistence/game game-id)
                 :game)
        player (-> game
                   :playermapping
                   user)
        pm (:player (engine/find-next-player-and-cards game))]
    (if (= pm player)
      (if (engine/check-moves-possible? game player cards)
        ;; TODO transactionality here? if check-moves-possible? was true but changed now?
        (persistence/moves-to-game! game-id (engine/store-moves game player cards))

        (log/error "can't do these moves!!!! fix me TODO"))
      ;; todo find out better way to handle this. this whole
      (log/error "player" player "tried to act on " pm " turn"))))

(defn check-game-end?
  "Check if all cards have been placed"
  [gid]
  (let [gstate (persistence/game gid)
        moves (:moves (:game gstate))
        all-placed-count (count (flatten (map vals (flatten (map vals (vals moves))))))
        player-count (count (keys moves))]
    (= (* player-count 13) all-placed-count)))

(defn- reverse-index
  " {:u (:p2), :m (:p1), :l (:p2)} -> ([:p2 :u] [:p1 :m] [:p2 :l])
  See http://blog.muhuk.com/2015/11/10/infinite_sequences_in_clojure.html#.XxsfuPhfjOQ"
  [m]
  (mapcat (fn [[k vs]] (zipmap vs (repeat k))) m))

(defn- add-winner-flag
  "Adds :winner true flag to :moves :u/m/l map.
  Expects inputs as follows in coll [:p1 :u] etc
  "
  [coll result]
  (if (empty? coll)
    result
    (let [[k1 k2] (first coll)
          restc (rest coll)
          agg (assoc-in result [k1 k2 :winner] true)]
      (recur restc agg))))

(defn- count-points
  "Counts points based on winners and hands.
  Example winners {:u [:p2], :m [:p1] :l [:p2]}
  Example result: {:p1 1 :p2 2}
  2021-04-29 17:42:38,766 [worker-3] INFO  chinese-poker.gamecontroller - count-points  ([:p1 :u] [:p1 :m] [:p1 :l])
  "
  [winners result]
  ;; TODO add bonus calculations
  (frequencies (flatten (vals winners))))

(defn- points->players
  "Map p1-4 to actual player id."
  [pointmap id->playermap]
  (into {} (for [p (keys pointmap)]
             [(p id->playermap) (p pointmap)])))

(defn- update-winners-and-results
  "Adds winner flag to :moves based on winners listing
  Example result: map keys {:playermapping :idtoplayermapping :dealt :moves :winners :points}
  Example winners {:u [:p2] :m [:p1] :l [:p2]}.
  Returns result with updated winners."
  [result]
  ;; TODO why reverse-index? this seems overly complex?
  (let [winners (reverse-index (:winners result))
        moves (:moves result)
        moves-with-winners (add-winner-flag winners moves)
        anonymous-points (count-points (:winners result) moves)
        points (points->players anonymous-points (:idtoplayermapping result))
        ]
    (-> result
        (assoc-in [:moves] moves-with-winners)
        (assoc-in [:points] points)
        )))

(defn get-next-cards
  "Gets next cards for given game-id"
  [game-id]
  ;; todo calculate whose turn it is based on placed cards.
  (when (nil? game-id)
    (log/error "get-next-cards called with nil game-id")
    (throw (RuntimeException. "get-next-cards called with nil game-id")))
  (let [game (:game (persistence/game game-id))
        pm (engine/find-next-player-and-cards game)
        p (:player pm)
        cc (:cardcount pm)
        n (if (zero? cc) 5 1)
        c (engine/get-n-cards-for-player game p n cc)
        id game-id
        moves (:moves game)
        ]
    {:cards c
     :id    id
     :moves moves
     :next  p
     }))

(defn- get-hand-id
  "Helper for getting playerid hand tuple from map entries"
  [entries kword]
  (map (fn [[k v]] [k (get-in v [kword :cards])]) entries))

(defn- map-to-hand
  "maps given map to display only to match valuator"
  [m]
  (map :display m))

(defn handle-same-hand-class
  "Expects to get same hand class hands and their indices. Returns ids of winners
  TODO how to find ties!
  TODO is this only used by test?"
  [sameclasshands handclass]
  (let [co (valuator/get-hand-comparator handclass)
        re (reverse (sort-by identity (fn [x y] (co (map :display (second x)) (map :display (second y)))) sameclasshands))
        res (take-while #(zero? (co (map :display (second (first re))) (map :display (second %)))) re)
        winning-hands (map second res)]
    (mapv first
          (filter (fn [x]
                    ((set winning-hands) (second x))) sameclasshands))))

(defn- get-winners-for-given-street
  "Handles given u/m/l hand winner, returns indices of best hands? or player ids?
  hand contains value and player id as a tuple?
  TODO naming, street?.

  what about ties?

  TODO think about the logic here; how hand valuation should
  happen. now it seems that first the hand class is resolved and if
  there are same classes then comparison is made for those.

  TODO dataformat in:
  ([:p1 [{:suit :diamond, :rank :a, :display AD}]])
  "
  [hands]
  (let [m (map (fn [[pid hand]] [pid hand (valuator/value (map-to-hand hand))]) hands)
        r (sort-by last > m)
        handclass (last (first r))
        same-class (take-while #(= handclass (last %)) r)]
    (if (= 1 (count same-class))
      (let [result (mapv first same-class)]
        result
        )
      ;; TODO think this last first again...
      (let [result (handle-same-hand-class same-class (last (first same-class)))]
        (log/debug "same-class result:" result)
        result))))

(defn handle-valid-hands
  "Handle 1- valid hands. stores winners. hand is {:u {:cards [c1 c2]} :m {...} :l {...}} map.
  Should probably contain player id?
  TODO is this only used by test?"
  [gamestate valid-hand-players]
  ;; TODO there probably is a more idiomatic way to handle this.
  ;; tassa halutaan nyt valid-hands (p1 p2) filtteroida gamestatesta ulos
  ;; aina vastaava pid ja kasi, ja sitten antaa voittajan ratketa per katu. onko tama edes jarkevin tapa lain miten tata tehda?
  (let [valid-games (filter #((first %) (set valid-hand-players)) (:moves gamestate))
        _ (log/info "valid-games" valid-games)
        winners {:u (get-winners-for-given-street (get-hand-id valid-games :u))
                 :m (get-winners-for-given-street (get-hand-id valid-games :m))
                 :l (get-winners-for-given-street (get-hand-id valid-games :l))
                 }]
    #_(log/debug "handle-valid-hands: winners:" winners)
    (assoc-in gamestate [:winners] winners)))

(defn- compare-same-class-first-wins?
  "Return true if first hand is better.
  Format for hands: collection of {:suit :heart, :rank :7, :display 7H}
  TODO implement. IS THIS NEEDED?"
  [h1 h2]
  (let [h1cards (map :display h1)
        h2cards (map :display h2)
        handclass (valuator/value h1cards)                  ;; this is enough, both should have the same
        handcomparator (valuator/get-hand-comparator handclass)
        result (pos? (handcomparator h1cards h2cards))]
    #_(log/debug "compare-same-class-first-wins?" h1 h2 "result:" result)
    result))

(defn- map-to-display
  "Maps hands to AD 2C etc"
  [m]
  (map :display m))

(defn- first-hand-wins?
  "Compare two hands and return true if first hand is better (or equal)"
  [{h1 :cards} {h2 :cards}]
  (let [v1 (valuator/value (map-to-display h1))
        v2 (valuator/value (map-to-display h2))]
    (if (> v1 v2)
      true
      (if (= v1 v2)
        (compare-same-class-first-wins? h1 h2)
        false))))

(defn- handle-no-valid-hands
  "Handle no valid hands. TODO implement"
  [_]
  (log/info "no valid hands, returning empty winners")
  {:type    :bf-game-ended
   :winners []
   })

(defn- correct-amount-of-cards-in-hands?
  "Check card amounts are 5/5/3"
  [{:keys [u m l]}]
  #_(log/info "correct amount:" u m l)
  (and (= 5 (count (:cards u)))
       (= 5 (count (:cards m)))
       (= 3 (count (:cards l)))))

(defn is-valid-hand-set?
  "check u>m>l.
   TODO is this only used by test?"
  [{:keys [u m l] :as h}]
  (let [u-m (first-hand-wins? u m)
        m-l (first-hand-wins? m l)]
    (and (and u-m m-l)
         (correct-amount-of-cards-in-hands? h))))

(defn get-valid-hands
  "Get valid hands.
   TODO is this only used by test?"
  [movescol]
  (is-valid-hand-set? movescol))

(defn get-results-for-game
  "Get results and retun gamestate with results.
   TODO is this only used by test?"
  [game]
  ;; first validate hands
  ;; draw the logical result tree. all valid/some valid/all invalid first
  ;; filter result based on hand validity first
  ;; todo mark invalid hands invalid in results!
  (let [valid-hands (map first (filter #(get-valid-hands (second %)) (:moves game)))]
    (log/info "get-result-for-game, valid hands:" valid-hands)
    (if (empty? valid-hands)
      (do
        (log/info "no valid hands:" (:moves game))
        (handle-no-valid-hands game))
      (handle-valid-hands game valid-hands))))

(defn get-results!
  "Get results and return gamestate with results."
  [gid]
  (let [game (persistence/game gid)
        tournament-id (:tournament game)
        _ (log/info "xxx got game:" game)
        result (get-results-for-game (:game game))
        endgame (update-winners-and-results result)]
    (persistence/moves-to-game! gid endgame)
    ;; TODO check if persistence is correct! (swap! games assoc-in [gid :game] endgame)
    #_(log/debug "get-results game:" game)
    (persistence/add-result-to-tournament! tournament-id endgame)
    endgame))
