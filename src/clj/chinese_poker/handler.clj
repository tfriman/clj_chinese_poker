(ns chinese-poker.handler
  (:require
    [chinese-poker.env :as env]
    [chinese-poker.layout :as layout]
    [chinese-poker.middleware :as middleware]
    [chinese-poker.routes.home :as home]
    [chinese-poker.routes.websockets :as websockets]
    [mount.core :as mount]
    [reitit.ring :as ring]
    [ring.middleware.content-type :as content-type]
    [ring.middleware.webjars :as webjars]))

(mount/defstate init-app
                :start ((or (:init env/defaults) (fn [])))
                :stop ((or (:stop env/defaults) (fn []))))

(mount/defstate app-routes
                :start
                (ring/ring-handler
                  (ring/router
                    [(home/home-routes)
                     websockets/websocket-routes])
                  (ring/routes
                    (ring/create-resource-handler
                      {:path "/"})
                    (content-type/wrap-content-type
                      (webjars/wrap-webjars (constantly nil)))
                    (ring/create-default-handler
                      {:not-found
                       (constantly (layout/error-page {:status 404, :title "404 - Page not found"}))
                       :method-not-allowed
                       (constantly (layout/error-page {:status 405, :title "405 - Not allowed"}))
                       :not-acceptable
                       (constantly (layout/error-page {:status 406, :title "406 - Not acceptable"}))}))))

(defn app []
  (middleware/wrap-base #'app-routes))
