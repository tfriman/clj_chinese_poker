(ns chinese-poker.core
  (:require
    [chinese-poker.config :as config]
    [chinese-poker.handler :as handler]
    [chinese-poker.nrepl :as nrepl]
    [cider.nrepl :refer [cider-nrepl-handler]]
    [clojure.tools.cli :as cli]
    [clojure.tools.logging :as log]
    [luminus.http-server :as http]
    [mount.core :as mount])
  (:gen-class))

;; TODO set me on (set! *warn-on-reflection* true)

;; log uncaught exceptions in threads
(Thread/setDefaultUncaughtExceptionHandler
  (reify Thread$UncaughtExceptionHandler
    (uncaughtException [_ thread ex]
      (log/error {:what      :uncaught-exception
                  :exception ex
                  :where     (str "Uncaught exception on" (.getName thread))}))))

(def cli-options
  [["-p" "--port PORT" "Port number"
    :parse-fn #(Integer/parseInt %)]])

(mount/defstate ^{:on-reload :noop} http-server
                :start
                (http/start
                  (-> config/env
                      (assoc :handler (handler/app))
                      (update :io-threads #(or % (* 2 (.availableProcessors (Runtime/getRuntime)))))
                      (update :port #(or (-> config/env :options :port) %))))
                :stop
                (http/stop http-server))

(mount/defstate ^{:on-reload :noop} repl-server
                :start
                (when (config/env :nrepl-port)
                  (nrepl/start {:bind    (config/env :nrepl-bind)
                                :port    (config/env :nrepl-port)
                                :handler cider-nrepl-handler}
                               ))
                :stop
                (when repl-server
                  (nrepl/stop repl-server)))

(defn stop-app []
  (doseq [component (:stopped (mount/stop))]
    (log/info component "stopped"))
  (shutdown-agents))

(defn start-app [args]
  (doseq [component (-> args
                        (cli/parse-opts cli-options)
                        mount/start-with-args
                        :started)]
    (log/info component "started"))
  (.addShutdownHook (Runtime/getRuntime) (Thread. ^Runnable stop-app)))

(defn -main [& args]
  (Thread/setDefaultUncaughtExceptionHandler
    (reify Thread$UncaughtExceptionHandler
      (uncaughtException [_ thread ex]
        (log/error ex "Uncaught exception on" (.getName thread)))))
  (start-app args))
