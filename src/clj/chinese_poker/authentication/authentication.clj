(ns chinese-poker.authentication.authentication
  (:require
    [chinese-poker.authentication.keycloak-v2 :as keycloak]
    [clojure.tools.logging :as log]))

;; Contains login stuff and stores logged in users etc

(defonce users-to-sessionids (atom {}))
(defonce sessionids-to-users (atom {}))

(defn login!
  [{:keys [token cookie]}]
  (log/info "login with session:" cookie)
  (let [{:keys [user email] :as access-token} (-> token
                         keycloak/verify
                         keycloak/extract)]
    (swap! users-to-sessionids assoc user cookie)
    (swap! sessionids-to-users assoc cookie user)))

(defn logout! [user]
  (log/info "logout " user)
  (let [uk (keyword user)
        ch (get @users-to-sessionids uk)]
    (swap! users-to-sessionids dissoc uk)
    (swap! sessionids-to-users dissoc ch)))
