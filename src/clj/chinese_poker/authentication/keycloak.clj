(ns chinese-poker.authentication.keycloak
  (:require [clj-http.client :as http]
            [clojure.java.io :as io]
            [mount.core :refer [defstate]])
  (:import (org.keycloak TokenVerifier)
           (org.keycloak.adapters KeycloakDeploymentBuilder)
           (org.keycloak.representations AccessToken)))

(defn- load-keycloak-deployment
  "take the keycloak configuration json file location on the classpath and return a KeycloakDeployment object"
  ([]
   (load-keycloak-deployment "keycloak.json"))
  ([keycloak-json-file]
   (with-open [keycloak-json-is (io/input-stream (io/resource keycloak-json-file))]
     (KeycloakDeploymentBuilder/build keycloak-json-is))))

(defstate keycloak-deployment
          :start (load-keycloak-deployment))

(defn- get-signing-key-id
  "Get first signing key from the well known endpoint"
  [oidc-discovery-url]
  (let [jwks-uri (-> (http/get oidc-discovery-url {:as :json})
                     :body
                     :jwks_uri)]
    ;; TODO add verification this returns something
    (->> (http/get jwks-uri {:as :json})
         :body
         :keys
         (filter #(and (= "sig" (:use %1)) (= "RS256" (:alg %1))))
         first
         :kid)))

(defn verify
  ([token]
   (verify keycloak-deployment token))
  ([deployment token]
   (def token token)
   (def deployment deployment)
   (let [kid (get-signing-key-id "http://localhost:8080/realms/chinese/.well-known/openid-configuration") ;; TODO put that in config file
         public-key (.getPublicKey (.getPublicKeyLocator deployment) kid deployment)]
     (-> (TokenVerifier/create token AccessToken)
         (.withDefaultChecks)
         (.realmUrl "http://localhost:8080/realms/chinese") ;; TODO get this from well known endpoint :issuer
         (.publicKey public-key)
         (.verify)
         (.getToken)
         ))))

(defn extract
  "return a map with :user and :email keys with values extracted from the Keycloak access token"
  [access-token]
  {:user  (.getPreferredUsername access-token)
   ;;:roles (set (map keyword (.getRoles (.getRealmAccess access-token))))
   :email (.getEmail access-token)})