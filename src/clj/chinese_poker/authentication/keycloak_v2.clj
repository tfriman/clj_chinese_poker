(ns chinese-poker.authentication.keycloak-v2
  (:require [clj-http.client :as http]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [mount.core :refer [defstate]])
  (:import (org.keycloak TokenVerifier)
           (org.keycloak.adapters KeycloakDeploymentBuilder)
           (org.keycloak.representations AccessToken)))

(defn- load-keycloak-deployment
  "take the keycloak configuration json file location on the classpath and return a KeycloakDeployment object"
  ([]
   (load-keycloak-deployment "keycloak.json"))
  ([keycloak-json-file]
   (with-open [keycloak-json-is (io/input-stream (io/resource keycloak-json-file))]
     (KeycloakDeploymentBuilder/build keycloak-json-is))))

(defn- get-signing-key-id
  "Get first signing key from the well known endpoint"
  [oidc-discovery-url]
  (let [jwks-uri (-> (http/get oidc-discovery-url {:as :json})
                     :body
                     :jwks_uri)]
    ;; TODO add verification this returns something
    (->> (http/get jwks-uri {:as :json})
         :body
         :keys
         (filter #(and (= "sig" (:use %1)) (= "RS256" (:alg %1))))
         first
         :kid)))

(defstate keycloak-deployment
          :start (let [deployment (load-keycloak-deployment)]
                   {:verify-fn  (fn [token] (let [kid (get-signing-key-id "http://localhost:8080/realms/chinese/.well-known/openid-configuration") ;; TODO put that in config file
                                                  public-key (.getPublicKey (.getPublicKeyLocator deployment) kid deployment)]
                                              (-> (TokenVerifier/create token AccessToken)
                                                  (.withDefaultChecks)
                                                  (.realmUrl "http://localhost:8080/realms/chinese") ;; TODO get this from well known endpoint :issuer
                                                  (.publicKey public-key)
                                                  (.verify)
                                                  (.getToken)
                                                  )))
                    :extract-fn (fn [access-token]
                                  {:user  (.getPreferredUsername access-token)
                                   :email (.getEmail access-token)})}))

(defn verify
  ([token]
   (verify keycloak-deployment token))
  ([deployment token]
   (def t token)
   (def d deployment)
   (log/info "verify " deployment)
   (-> token
       ((:verify-fn deployment)))))

(defn extract
  "return a map with :user and :email keys with values extracted from the Keycloak access token"
  [access-token]
  (log/info "extract" access-token)
  (-> access-token
      ((:extract-fn keycloak-deployment))))