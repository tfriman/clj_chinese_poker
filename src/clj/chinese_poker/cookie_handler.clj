(ns chinese-poker.cookie-handler)
;; Contains cookie handling stuff

(def cookie-name "ACookieX")

(defn get-id-cookie-value
  "Gets cookie from map"
  [in-map]
  (-> (get in-map cookie-name)
      first
      second
      ))

(comment
  ;; test
  (= "QWxhZGRpbjpPcGVuU2VzYW1l" (get-id-cookie-value {"ring-session" {:value "e9056c41-b2e5-4806-a75b-a2012155dafc"}, "ACookieX" {:value "QWxhZGRpbjpPcGVuU2VzYW1l"}}))
  )
