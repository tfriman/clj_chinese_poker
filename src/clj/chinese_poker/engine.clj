(ns chinese-poker.engine
  (:require [chinese-poker.poker.deck :as deck]
            [clojure.set :as set]
            [clojure.tools.logging :as log]
            [mount.core :as mount]))

;; This will handle game logic

(defn- player-turns
  "Creates order for players based on the game round. sort by :p1 - 4"
  [players seed]
  (let [playercount (count players)
        mapping (zipmap players (take playercount (drop (* seed (dec playercount)) (cycle (take playercount [:p1 :p2 :p3 :p4])))))]
    (into (sorted-map-by (fn [key1 key2]
                           (* -1 (compare [(get mapping key2) key2]
                                          [(get mapping key1) key1]))))
          mapping)))

(defn- dealt-hands
  "Generate hands"
  [deck playercount]
  (loop [pindx 1
         d deck
         result {}]
    (if (= (inc playercount) pindx)
      result
      (recur (inc pindx) (drop 13 d) (assoc result (keyword (str "p" pindx)) (vec (take 13 d)))))))

(defn create-game-fn
  "Creates game."
  [next-deck-fn]
  (fn [n players roundnumber]
    (let [d (next-deck-fn)
          dealt (dealt-hands d (count players))
          playermapping (player-turns players roundnumber)]
      {:playermapping     playermapping
       :idtoplayermapping (set/map-invert playermapping)
       :dealt             dealt
       :moves             (select-keys {:p1 {:u {:cards []}
                                             :m {:cards []}
                                             :l {:cards []}
                                             }
                                        :p2 {:u {:cards []}
                                             :m {:cards []}
                                             :l {:cards []}
                                             }
                                        :p3 {:u {:cards []}
                                             :m {:cards []}
                                             :l {:cards []}
                                             }
                                        :p4 {:u {:cards []}
                                             :m {:cards []}
                                             :l {:cards []}
                                             }
                                        }
                                       (keys dealt))})))

(mount/defstate create-game :start (create-game-fn (fn [] (shuffle deck/deck))))

(defn- makeplayer-symbol-from-int
  "makes i symbol if it isn't already."
  [i]
  (if (keyword? i)
    i
    (keyword (str "p" i))))

(defn get-n-cards-for-player
  "Gets n cards from game g for player p starting from position o"
  ;; TODO player naming system here?
  [g p n o]
  {:post [(vector? %)]}
  (->> (take n (drop o ((makeplayer-symbol-from-int p) (:dealt g))))
       (into [])))

(defn find-next-player-and-cards
  "Finds who is the next player and how many cards she has placed already.
  Returns map with keys :player :cardcount.
  Logic: calculate played cards and then sort, smallest or first wins."
  [m]
  (let [moves (:moves m)
        player-card-count-map (->> moves
                                   (map #(select-keys (second %) [:u :m :l]))
                                   (map vals)
                                   (map #(map vals %))
                                   (map (comp count flatten))
                                   (zipmap (keys moves)))
        sortlist (sort-by second player-card-count-map)
        p (first sortlist)
        ;; TODO xxx this should take into account :playermapping
        ]
    {:player    (first p)
     :cardcount (second p)}))

(defn- make-moves-map
  "seq of card+:u/:m/:l pairs to map { :u [...] etc}"
  [m]
  (apply merge
         (map #(hash-map (first %) (hash-map :cards (map first (second %)))) (group-by second m)))
  ;;(reduce (partial merge-with into) (map #(hash-map (second %) [(first %)]) m))
  )

(defn check-moves-possible?
  "Check if player can do moves"
  [gamestore player new-moves]

  (let [m (player (:moves gamestore))
        _ (def xxx {:m m :player player :nm new-moves})
        mer (merge-with (partial merge-with into) m new-moves)
        u (count (get-in mer [:u :cards]))
        m (count (get-in mer [:m :cards]))
        l (count (get-in mer [:l :cards]))
        allkeycount (count (keys mer))]
    (if (or (> u 5) (> m 5) (> l 3) (> allkeycount 3))
      (do
        (log/error "NOPES! u " u " m " m " l " l " all keys:" allkeycount)
        false)
      true)))

(defn store-moves
  "Store cards to hands for player p
   moves-new:  {:u [cards...] :m [cards] :l [cards]}
   Returns new game"
  [g p moves-new]
  (let [player (makeplayer-symbol-from-int p)
        moves-old (player (:moves g))
        moves-m (merge-with (partial merge-with into) moves-old moves-new)]
    (assoc-in g [:moves player] moves-m)))
