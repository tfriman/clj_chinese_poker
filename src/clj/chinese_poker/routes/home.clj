(ns chinese-poker.routes.home
  (:require [chinese-poker.authentication.authentication :as auth]
            [chinese-poker.config :as config]
            [chinese-poker.cookie-handler :as cookie]
            [chinese-poker.layout :as layout]
            [chinese-poker.middleware :as middleware]
            [clojure.tools.logging :as log]
            [chinese-poker.helpers :as helpers]
            [ring.util.http-response :as response]))

(defn home-page [request]
  (layout/render request "home.html"))

(defn login-page [request]
  (let [body (helpers/json-string->map (slurp (.bytes ^org.httpkit.BytesInputStream (:body request))))
        c (cookie/get-id-cookie-value (:cookies request))]
    (log/info "login called with " body)
    (auth/login! {:cookie c :token (:auth-token body)})
    )
  ;; TODO login always success, fix this
  (response/ok {:login "ok"}))

(defn logout-page [request]
  (let [body (helpers/json-string->map (slurp (.bytes ^org.httpkit.BytesInputStream (:body request))))
        u (:username body)
        c (cookie/get-id-cookie-value (:cookies request))]
    (log/info "Logout user: " u " with cookie:" c)
    (auth/logout! u))
  ;; TODO logout always success...
  (response/ok {:logout "ok"}))

(defn config-page
  "This provides configs for the spa ui."
  [_]
  (response/ok (:spaconfig config/env)))

(defn auth-setup [request]
  (def req request)
  nil
  )

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/login" {:post login-page}]
   ["/logout" {:post logout-page}]
   ["/" {:get home-page}]
   ["/auth/github" {:get auth-setup}]
   ["/spaconfig.json" {:get config-page}]
   ])
