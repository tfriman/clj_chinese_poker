(ns chinese-poker.routes.websockets
  (:require [chinese-poker.gamecontroller :as controller]
            [chinese-poker.helpers :as helpers]
            [clojure.tools.logging :as log]
            [org.httpkit.server :as httpkit]
            [chinese-poker.authentication.keycloak-v2 :as keycloak]))

(defonce channels (atom #{}))
(defonce players (atom {}))
(defonce channels-to-players (atom {}))
(defonce message-flow (atom '()))                           ;; TODO remove me

(defn- store-creds!
  "Store user info to channel"
  [player channel]
  (log/info "store-creds! player:" player "channel" channel)
  (swap! players assoc player {:channel channel})
  (swap! channels-to-players assoc channel player))

(defn- get-player
  "Gets player from channel"
  [chan]
  (let [res (get @channels-to-players chan :player-not-found)]
    res))

(defn disconnect! [channel _]
  ;; TODO remove player from active as well
  (swap! channels #(set (remove #{channel} %)))
  (let [pc (get channels-to-players channel)
        _ (log/info "disconnect found player " pc)]
    (swap! players (fn [x] (dissoc x pc)))
    (swap! channels-to-players (fn [x] (dissoc x channel)))))

(defn- send-to-channels
  "Broadcasts message to given channels"
  [msg channels]
  (let [m (helpers/msg-as-json msg)]
    (swap! message-flow conj (assoc msg :inout :out))
    (doseq [channel channels]
      (httpkit/send! channel m))))

(defn connect! [channel]
  (swap! channels conj channel)
  (send-to-channels {:type :bf-credential-req} (list channel)))

(defn- get-channels
  "Helper to wrap atom. Gets channels for players"
  [player-seq]
  (map :channel (vals (select-keys @players player-seq))))

(defn send-to-tournament
  "Send messages to everyone in a given tournament."
  [msg tournament-id]
  (let [names (controller/get-player-names-for-tournament tournament-id)
        channels (get-channels names)]
    #_(log/debug "send-to-tournament " (:type msg) " " tournament-id " and channels " (count channels))
    (send-to-channels msg channels)))

(defn- get-next-cards
  "Gets next cards and send them"
  [game-id]
  (let [{:keys [cards id moves next]} (controller/get-next-cards game-id)]
    #_(log/debug "sending to" next " cards:" cards)
    {:type    :bf-place-cards
     :cards   cards
     :game-id id
     :moves   moves
     :next    next
     }))

(defmulti handle-msg!
          (fn [_ msg]
            ;;{:pre [(protocol/valid-msg? msg)]} ;; SPEC for incoming messages
            (log/info "dispatch handle-msg!" msg)
            (:type msg)))

(defn handle-channel-recv!
  "Receives req, parses it to a msg map and invokes the msg handler"
  [chan req]
  (when (= :player-not-found (get-player chan))
    (log/error "Call was INVALID, no player found for " chan))
  (let [msg (helpers/msg->map req)]
    (swap! message-flow conj (assoc msg :inout :in))
    (log/info "handle message " (:type msg))
    (log/debug "raw message:" (:type msg) req)
    (handle-msg! chan msg)))

(defn- ongoing-tournament
  "Search possible ongoing tournament."
  [user]
  (controller/ongoing-tournaments user))

(defmethod handle-msg! :fb-credentials
  [chan {:keys [user token]}]

  ;; TODO this should close the channel if token verification fails.
  (def token token)
  (let [user-from-token (-> (keycloak/verify token)
                            keycloak/extract
                            :user
                            keyword)]
    (assert (= user user-from-token) (str "user and token user do not match: '" user "' and token user: '" user-from-token "'"))
    (store-creds! user-from-token chan))
  ;; fetch possible games ongoing
  (let [tournament (ongoing-tournament user)
        open-tournaments (controller/open-tournaments)]
    (send-to-channels {:type                 :bf-login-info
                       :on-going-tournaments tournament
                       :open-tournaments     open-tournaments} [chan])))

(defmethod handle-msg! :fb-create-tournament
  [_ msg]
  (let [tournament (controller/create-tournament (select-keys msg [:user :name]))]
    (log/info ":fb-create-tournament called:" msg "created:" tournament)
    (send-to-channels {:type    :bf-tournament-created
                       :id      tournament
                       :name    (:name msg)
                       :creator (:user msg)
                       }
                      ;; TODO remove channels
                      @channels)))

(defmethod handle-msg! :fb-start-tournament
  [_ msg]
  #_(log/debug "handle-msg! :start-tournament " msg)
  (controller/start-tournament (select-keys msg [:user :tournament-id]))
  (let [tournament-id (:tournament-id msg)]
    #_(log/infof ":start-tournament called to tournament id: %s" (:tournament-id msg))
    (send-to-tournament {:type          :bf-tournament-started
                         :creator       (:user msg)
                         :tournament-id tournament-id
                         }
                        tournament-id)))

(defmethod handle-msg! :fb-join-tournament
  [_ {:keys [tournament-id user]}]
  (let [tournament-players (controller/join-tournament (helpers/keywordize-if-not-already user) tournament-id)]
    (swap! players assoc-in [user :active-tournament] tournament-id)
    (send-to-tournament {:type          :bf-player-joined
                         :player        user
                         :players       tournament-players
                         :tournament-id tournament-id}
                        tournament-id)))

#_(defmethod handle-msg! :fb-rejoin-tournament
    [chan {:keys [user tournament-id]}]
    (def user user)                                         ;; todo remove
    (def tournament-id tournament-id)                       ;; todo remove
    (send-to-tournament                                     ;; TODO should this be send-to-tournaments?
      {:type :foo
       :data {}  #_(controller/rejoin-tournament user tournament-id)}
      tournament-id
      ))

(defmethod handle-msg! :fb-fetch-game
  [_ {:keys [game-id user]}]
  (let [game (controller/fetch-game game-id)
        chan (-> @players user :channel)]
    (if game
      (do

        (log/info "fb-fetch-game found:" game)
        (let [outgoing (-> (get-next-cards game-id)
                           ;; TODO this needs some reconsideration. This basically bundles 3 messages together.
                           (assoc :players (-> game :game :idtoplayermapping))
                           (assoc :type :bf-game-found)
                           (assoc :tournament (:tournament game)))]
          (send-to-channels outgoing [chan]))
        )
      (do
        (log/warn "fetch did not find game:" game-id)       ;; TODO is this a good idea?
        (send-to-channels {:type    :bf-game-not-found
                           :game-id game-id} [chan])))))

(defmethod handle-msg! :fb-start-game
  ;; "TODO remove me or at least remove two controller calls. When is this even called?"
  [_ {:keys [tournament-id user]}]
  (log/info ":fb-start-game " user " tournament" tournament-id)
  (let [game (controller/start-game {:user          user
                                    :tournament-id tournament-id})]
    (send-to-tournament game tournament-id)))

(defmethod handle-msg! :fb-ready-for-game
  ;; todo this is actually not used at all?
  [_ {:keys [tournament-id user]}]
  #_(log/info "handle-msg! :c-ready-for-game " msg)
  (let [game-ready (controller/mark-player-ready user tournament-id)]
    (def game-ready game-ready)
    (case (:status game-ready)
      :ready (do (log/infof "players are ready for tournament %s" tournament-id)
                 (send-to-tournament (:game game-ready) tournament-id))
      :waiting (log/infof "game is waiting for %s" tournament-id))))

(defn- count-total-points
  "TODO move me"
  [tournament]
  (reduce
    (partial merge-with +)
    (map :points (:results tournament))))

;; TODO refactor me
(def game-ended-with-points (atom ()))

(defmethod handle-msg! :fb-cards-placed
  [_ msg]
  (log/debug "handle-msg! :cards-placed " msg)
  (let [game-id (:game-id msg)
        tournament-id (:tournament-id msg)]
    (if (controller/placed-cards-handler (select-keys msg [:user :cards :game-id]))
      ;; TODO change to get keys to make format unified. when to send these?
      ;; TODO should this be game-id?
      (if (controller/check-game-end? game-id)
        (let [{:keys [winners points] :as results} (controller/get-results! game-id)
              tournament (controller/tournament-status tournament-id)]
          (log/info "game ended, game-id:" game-id " and msg" msg)
          (log/info "game ended, results:" results " tournament " tournament)
          (when (not-empty winners) (swap! game-ended-with-points conj msg))
          (send-to-tournament {:type    :bf-game-ended
                               :winners (:winners results)
                               :moves   (:moves results)
                               :game-id game-id
                               }
                              tournament-id)
          (log/infof "total points for tid %s are %s" tournament-id (count-total-points tournament))

          (send-to-tournament {:type          :bf-tournament-update
                               :tournament-id (helpers/keywordize-if-not-already tournament-id)
                               :game-id       game-id
                               ;;:winners winners
                               ;; TODO add actual results here
                               :previous      (merge winners {:points points})
                               :totalpoints   (count-total-points tournament)}
                              tournament-id))
        (send-to-tournament (get-next-cards game-id) tournament-id))
      (do
        (log/error "something was off, cards could not be set like this!")
        (send-to-tournament {:type    :input-error
                             :message "you sent something wrongly"} tournament-id))
      )
    ))

(defn ws-handler [request]
  (httpkit/as-channel request
                      {:on-open    connect!
                       :on-close   disconnect!
                       :on-ping    (fn [ch data] (httpkit/send! ch (str "ECHO: " data)))
                       :on-receive handle-channel-recv!}
                      ))
(def websocket-routes
  ["/ws" ws-handler])
