(ns chinese-poker.persistence.persistence
  (:require [clojure.tools.logging :as log]))

(defonce a-tournaments (atom {}))

(defonce a-games (atom {}))

(defn ongoing-tournaments-for-player
  "Fetch ongoing tournaments as seq for given player"
  [player]
  (filter (fn [t] (some #(= player %) (:players t))) (map second @a-tournaments)))

(defn tournaments
  "Returns all tournaments as a map"
  []
  @a-tournaments)

(defn tournament
  "get one tournament by tournament id"
  [tournament-id]
  (get @a-tournaments tournament-id))

(defn player-ready!
  "Marks player ready for tournament, return given tournament"
  ^{:pre [(keyword? tournament-id) (keyword? player-id)]}
  [tournament-id player-id]
  (-> (swap! a-tournaments update-in [tournament-id :ready-count] conj player-id)
      tournament-id))

(defn new-tournament
  "Store a tournament and return it."
  ^{:pre [(keyword? (:id tournament))]}
  [{id :id :as tournament}]
  (-> (swap! a-tournaments assoc id tournament)
      id))

(defn game
  "Gets a game"
  [game-id]
  (game-id @a-games))

(defn update-tournament-status
  "Updates tournament status, returns tournament"
  ^{:pre [(keyword? tournament-id)]}
  [tournament-id status]
  (-> (swap! a-tournaments update-in [tournament-id :status] (fn [_] status))
      tournament-id))

(defn player-to-tournament
  "Add player to tournament and return tournament, returns updated player list"
  ^{:pre [(keyword? tournament-id) (keyword? player-id)]}
  [tournament-id player-id]
  (doto
    (-> (swap! a-tournaments update-in [tournament-id :players] (fnil conj []) player-id)
        tournament-id
        :players)
    ((fn [x] (println "Added players player-to-tournament " x)))))

(defn moves-to-game!
  "Replace moves, returns game"
  ^{:pre [(keyword? game-id)]}
  [game-id moves]
  (-> (swap! a-games assoc-in [game-id :game] moves)
      game-id))

(defn mark-tournament-ready!
  "todo is this needed. returns tournament."
  ^{:pre [(keyword? tournament-id)]}
  [tournament-id]
  (-> (swap! a-tournaments update-in [tournament-id :ready-count] (fn [_] #{}))
      tournament-id))

(defn store-game!
  "Store game, update tournament to have this as an ongoing and returns the same game.
  TODO let this decide id and return whole stored doc?"
  ^{:pre [(keyword? (:id game)) (keyword? (:tournament game))]}
  [{id :id tournament :tournament :as game}]
  (log/info "Game being stored with id:" id)
  (swap! a-tournaments (fn [m] (update m tournament #(assoc % :ongoing-game-id id)))) ;; this is not atomic
  (swap! a-games assoc id game)
  game)

(defn add-result-to-tournament!
  "Adds the whole game to results, returns given tournament."
  ^{:pre [(keyword? tournament-id)]}
  [tournament-id result]
  (log/info "add-result-to-tournament!" tournament-id " result " result)
  (swap! a-tournaments (fn [m] (update m tournament #(dissoc % :ongoing-game-id)))) ;; this is not atomic.
  (-> (swap! a-tournaments update-in [tournament-id :results] conj result)
      tournament-id))
